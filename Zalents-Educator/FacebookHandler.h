//
//  FacebookHandler.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 14/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^onCompletion)(BOOL);
typedef void(^onComplete)(NSArray*);

@interface FacebookHandler : NSObject

@property (nonatomic, strong) UIViewController *currentViewController;

-(void)fbLoginEvent:(onCompletion)onCompletion;
-(void)fbSignUpEvent:(onComplete)results;

@end
