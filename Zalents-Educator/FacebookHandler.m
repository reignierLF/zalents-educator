//
//  FacebookHandler.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 14/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "FacebookHandler.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@implementation FacebookHandler

-(instancetype)init{
    
    self = [super init];
    
    if(self){
        UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
        vc = [vc.childViewControllers lastObject];
        
        _currentViewController = vc;
    }
    
    return self;
}

-(void)fbLoginEvent:(onCompletion)onCompletion{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email"] fromViewController:_currentViewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
            NSLog(@"error %@",error);
        } else if (result.isCancelled) {
            // Handle cancellations
            NSLog(@"Cancelled");
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                onCompletion(YES);
                NSLog(@"%@",result);
                NSLog(@"Correct");
            }
        }
    }];
}

-(void)fbSignUpEvent:(onComplete)results{
    
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"first_name,last_name,email,gender,birthday" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                  id result, NSError *error) {
         
         NSLog(@"check");
         
         __weak typeof(self) weakSelf = self;
         
         if(result == nil){
             [weakSelf fbLoginEvent:^(BOOL onCompletion){
                 if(onCompletion){
                     [weakSelf fbSignUpEvent:^(NSArray* result){
                         results(result);
                     }];
                 }
             }];
             
             NSLog(@"not yet login");
         }else{
             NSLog(@"result t = %@",result);
             results(result);
             NSLog(@"already login");
         }
     }];
}

@end
