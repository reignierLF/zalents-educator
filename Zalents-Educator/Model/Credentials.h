//
//  Credentials.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 29/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Credentials : NSObject

@property (nonatomic, readonly) NSString *accessCode;
@property (nonatomic, readonly) NSString *authenticationToken;
@property (nonatomic, readonly) NSString *birthday;
@property (nonatomic, readonly) NSString *countryCode;
@property (nonatomic, readonly) NSString *credit;
@property (nonatomic, readonly) NSString *email;
@property (nonatomic, readonly) NSString *firstName;
@property (nonatomic, readonly) NSString *gender;
@property (nonatomic, readonly) NSString *idCredential;
@property (nonatomic, readonly) NSString *lastName;
@property (nonatomic, readonly) NSString *mobileNumber;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
