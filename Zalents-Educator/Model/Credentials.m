//
//  Credentials.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 29/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "Credentials.h"

@implementation Credentials

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        _accessCode = [dic valueForKey:@"access_code"];
        _authenticationToken = [dic valueForKey:@"authentication_token"];
        _birthday = [dic valueForKey:@"birthdate"];
        _countryCode = [dic valueForKey:@"country_code"];
        _credit = [dic valueForKey:@"credit"];
        _email = [dic valueForKey:@"email"];
        _firstName = [dic valueForKey:@"first_name"];
        _idCredential = [dic valueForKey:@"id"];
        _lastName = [dic valueForKey:@"last_name"];
        _mobileNumber = [dic valueForKey:@"mobile_number"];
        
        //[self setCredentials:_accessCode key:@"access_code"];
        [self setCredentials:_authenticationToken key:@"authentication_token"];
        [self setCredentials:_birthday key:@"birthdate"];
        [self setCredentials:_countryCode key:@"country_code"];
        [self setCredentials:_credit key:@"credit"];
        [self setCredentials:_email key:@"email"];
        [self setCredentials:_firstName key:@"first_name"];
        [self setCredentials:_idCredential key:@"id"];
        [self setCredentials:_lastName key:@"last_name"];
        [self setCredentials:_mobileNumber key:@"mobile_number"];
    }
    
    return self;
}

-(void)setCredentials:(id)param key:(NSString*)key{
    [[NSUserDefaults standardUserDefaults] setObject:param forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"save credentials : %@ (KEY:%@)", param,key);
}

@end
