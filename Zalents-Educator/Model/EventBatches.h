//
//  EventBatches.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventBatches : NSObject

@property (nonatomic, strong) NSArray *eventBatchesId;
@property (nonatomic, strong) NSArray *eventBatchesCutoffAt;
@property (nonatomic, strong) NSArray *eventBatchesDuration;
@property (nonatomic, strong) NSArray *eventBatchesEndTime;
@property (nonatomic, strong) NSArray *eventBatchesEndAt;
@property (nonatomic, strong) NSArray *eventBatchesEventId;
@property (nonatomic, strong) NSArray *eventBatchesSlot;
@property (nonatomic, strong) NSArray *eventBatchesStartTime;
@property (nonatomic, strong) NSArray *eventBatchesStartedAt;
@property (nonatomic, strong) NSArray *eventBatchesStatus;
@property (nonatomic, strong) NSArray *eventBatchesVenue;
@property (nonatomic, strong) NSArray *eventBatchesVenue1;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
