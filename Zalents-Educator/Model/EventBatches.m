//
//  EventBatches.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "EventBatches.h"
#import "TimeConverter.h"

@interface EventBatches ()

@property (nonatomic, strong) TimeConverter *tc;

@end

@implementation EventBatches

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _tc = [[TimeConverter alloc] init];
        
        _eventBatchesId = [self refineData:[dic valueForKey:@"id"]];
        _eventBatchesCutoffAt = [dic valueForKey:@"cut_off_at"];
        _eventBatchesDuration = [self refineData:[dic valueForKey:@"duration"]];
        
        _eventBatchesStartedAt = [self refineDate:[dic valueForKey:@"started_at"]];
        _eventBatchesEndAt = [self refineDate:[dic valueForKey:@"ended_at"]];
        
        _eventBatchesStartTime = [self refineData:[dic valueForKey:@"start_time"]];
        _eventBatchesEndTime = [self refineData:[dic valueForKey:@"end_time"]];
        
        _eventBatchesEventId = [self refineData:[dic valueForKey:@"event_id"]];
        _eventBatchesSlot = [self refineData:[dic valueForKey:@"slot"]];
        _eventBatchesStatus = [self refineData:[dic valueForKey:@"status"]];
        _eventBatchesVenue = [self refineData:[dic valueForKey:@"venue"]];
        _eventBatchesVenue1 = [self refineData:[dic valueForKey:@"venue_1"]];
    }
    
    return self;
}

-(NSArray*)refineData:(NSArray*)array{
    
    if(array != (id)[NSNull null] || array != nil){
        
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        
        if(array.count == 0){
            [tempArray addObject:@"N/A"];
        }else{
            for (int i = 0; i < array.count; i++) {
                
                NSString *newString;
                
                if([array objectAtIndex:i] == (id)[NSNull null]){
                    newString = @"N/A";
                }else{
                    newString = [NSString stringWithFormat:@"%@",[array objectAtIndex:i]];
                }
                
                [tempArray addObject:newString];
            }
        }
        
        array = tempArray;
    }
    
    return array;
}

-(NSArray*)refineDate:(NSArray*)array{
    
    if(array != (id)[NSNull null] || array != nil){
        
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        
        if(array.count == 0){
            
            [tempArray addObject:@"N/A"];
        }else{
            for (int i = 0; i < array.count; i++) {
                
                NSString *newString;
                
                if([array objectAtIndex:i] == (id)[NSNull null]){
                    newString = @"N/A";
                }else{
                    newString = [_tc serverDateFormatToDate:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
                }
                
                [tempArray addObject:newString];
            }
        }
        
        array = tempArray;
    }
    
    return array;
}

-(NSArray*)refineTime:(NSArray*)array{
    
    if(array != (id)[NSNull null] || array != nil){
        
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        
        if(array.count == 0){
            
            [tempArray addObject:@"N/A"];
        }else{
            for (int i = 0; i < array.count; i++) {
                
                NSString *newString;
                
                if([array objectAtIndex:i] == (id)[NSNull null]){
                    newString = @"N/A";
                }else{
                    
                    newString = [_tc serverTimeFormatToDayTime:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
                }
                
                [tempArray addObject:newString];
            }
        }
        
        array = tempArray;
    }
    
    return array;
}

@end
