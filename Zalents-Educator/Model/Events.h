//
//  Events.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "EventBatches.h"

@interface Events : NSObject

@property (nonatomic, strong) NSString *eventsId;
@property (nonatomic, strong) NSString *eventsImageLargeUrl;
@property (nonatomic, strong) NSString *eventsImageMediumUrl;
@property (nonatomic, strong) NSString *eventsImageSmallUrl;
@property (nonatomic, strong) NSString *eventsTitle;
@property (nonatomic, strong) NSString *eventsCredit;
@property (nonatomic, strong) NSString *eventsDescription;

@property (nonatomic, strong) EventBatches *eventBatches;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
