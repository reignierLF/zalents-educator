//
//  Events.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "Events.h"

@implementation Events

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        _eventsId = [dic valueForKey:@"id"];
        //_eventsImageLargeUrl = [dic valueForKey:@"image_large_url"];
        //_eventsImageMediumUrl = [dic valueForKey:@"image_medium_url"];
        //_eventsImageSmallUrl = [dic valueForKey:@"image_small_url"];
        _eventsImageLargeUrl = [self checkImageUrl:[dic valueForKey:@"image_large_url"]];
        _eventsImageMediumUrl = [self checkImageUrl:[dic valueForKey:@"image_medium_url"]];
        _eventsImageSmallUrl = [self checkImageUrl:[dic valueForKey:@"image_small_url"]];
        _eventsTitle = [self checkTitle:[dic valueForKey:@"title"]];
        _eventsCredit = [self checkCredit:[dic valueForKey:@"credit"]];
        _eventsDescription = [self checkDescription:[dic valueForKey:@"description"]];
        
        _eventBatches = [[EventBatches alloc] initWithDictionary:[dic valueForKey:@"event_batches"]];
    }
    
    return self;
}

-(NSString*)checkImageUrl:(id)param{
    
    if(param == (id)[NSNull null] || param == nil){
        param = @"noImage";
    }
    
    return param;
}

-(NSString*)checkTitle:(id)param{
    
    if(param == (id)[NSNull null] || param == nil){
        
        param = @"No Title available";
    }
    
    //NSLog(@"param 1 = %@",param);
//    }else{
//        
//        if([param isEqualToString:@""]){
//            param = @"No Title available";
//        }
//    }
    
    return param;
}

-(NSString*)checkCredit:(id)param{
    
    if(param == (id)[NSNull null] || param == nil){
        
        param = @"0";
    }
    
    //NSLog(@"param 2 = %@",param);
//    }else{
//        
//        if([param isEqualToString:@""]){
//            param = @"0";
//        }
//    }
    
    return param;
}

-(NSString*)checkDescription:(id)param{
    
    if(param == (id)[NSNull null] || param == nil){
        
        param = @"No Description";
    }else{
        
        if([param isEqualToString:@""]){
            
            param = @"No Description";
        }
    }

    return param;
}

@end
