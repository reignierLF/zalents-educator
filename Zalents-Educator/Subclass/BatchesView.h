//
//  BatchesView.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 16/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BatchesView;

@protocol BatchesViewDelegate

-(void)didShowDatesChoices;

-(void)paymentEvent:(BatchesView*)batchesView;

@end

@interface BatchesView : UIView

@property (nonatomic, readonly) float screenWidth;
@property (nonatomic, readonly) float screenHeight;
@property (nonatomic, readonly) UIView *relativeView;

@property (nonatomic, strong) UIButton *bookButton;
@property (nonatomic) float iconSize;

@property (nonatomic, readwrite) NSTextAlignment textAlignment;
@property (nonatomic, readwrite) UIColor *textColor;
@property (nonatomic, readwrite) UIFont *font;
@property (nonatomic, readwrite) NSArray *textArray;
@property (nonatomic, readwrite) NSArray *iconArray;
@property (nonatomic, readwrite) NSArray *labelArray;

@property (nonatomic, weak) id <BatchesViewDelegate> delegate;

-(instancetype)initWithRelativeView:(UIView*)view;

@end
