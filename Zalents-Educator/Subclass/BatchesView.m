//
//  BatchesView.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 16/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "BatchesView.h"

@implementation BatchesView

-(instancetype)initWithRelativeView:(UIView*)view{
    
    self = [super init];
    
    if(self){
        
        _screenWidth = view.frame.size.width;
        _screenHeight = view.frame.size.height;
        
        _relativeView = view;
        
        //self.backgroundColor = [UIColor whiteColor];
    }
    
    return self;
}

-(void)didMoveToSuperview{

    //[self initOldVersion];
    [self initNewVersion];
}

-(void)initOldVersion{
    if(_textColor == nil){
        _textColor = [UIColor blackColor];
    }
    
    if(_font == nil){
        _font = [UIFont fontWithName:@"Roboto-Regular" size:12];
    }
    
    if(_iconArray == nil){
        _iconArray = @[@"s_calendar", @"s_info", @"s_search", @"s_compass"];
    }
    
    int index = 0;
    
    for (int i = 0; i < 2; i++) {
        
        index = i;
        
        for (int j = 0; j < 2; j++) {
            
            index += (j + 1);
            
            UIView *cellContainerView = [[UIView alloc] initWithFrame:CGRectMake(10 + (((_screenWidth - 20) / 2) * i), ((_screenHeight - 30) / 2) * j, (_screenWidth - 20) / 2, (_screenHeight - 30) / 2)];
            cellContainerView.backgroundColor = [UIColor whiteColor];
            cellContainerView.layer.borderWidth = 1;
            cellContainerView.layer.borderColor = [UIColor grayColor].CGColor;
            [self addSubview:cellContainerView];
            
            UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (cellContainerView.frame.size.height / 2) - (_iconSize / 2), _iconSize, _iconSize)];
            //iconImageView.backgroundColor = [UIColor greenColor];
            iconImageView.image = [UIImage imageNamed:[_iconArray objectAtIndex:index - 1]];
            iconImageView.contentMode = UIViewContentModeScaleAspectFill;
            iconImageView.clipsToBounds = YES;
            [cellContainerView addSubview:iconImageView];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(iconImageView.frame.origin.x + iconImageView.frame.size.width + 10, 10, cellContainerView.frame.size.width - (iconImageView.frame.origin.x + iconImageView.frame.size.width + 20), cellContainerView.frame.size.height - 20)];
            //label.backgroundColor = [UIColor cyanColor];
            label.text = [NSString stringWithFormat:@"%@",[_textArray objectAtIndex:index - 1]];
            label.tag = i;
            label.textAlignment = _textAlignment;
            label.textColor = _textColor;
            label.font = _font;
            label.numberOfLines = 0;
            label.lineBreakMode = NSLineBreakByWordWrapping;
            [cellContainerView addSubview:label];
        }
    }
    
    _bookButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _bookButton.frame = CGRectMake(10, _screenHeight - 30, _screenWidth - 20, 30);
    _bookButton.backgroundColor = [UIColor blueColor];
    [_bookButton setTitle:@"Book Now" forState:UIControlStateNormal];
    [_bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:_bookButton];
    
    [_bookButton addTarget:self action:@selector(paymentEvent) forControlEvents:UIControlEventTouchUpInside];
}

-(void)initNewVersion{
    
    float cellHeight = 40;
    
    if(_textColor == nil){
        _textColor = [UIColor blackColor];
    }
    
    if(_font == nil){
        _font = [UIFont systemFontOfSize:12];
    }
    
    if(_iconArray == nil){
        _iconArray = @[@"b_calendar", @"b_clock", @"b_slots", @"b_venue"];
    }
    
    NSMutableArray *labelMutableArray = [[NSMutableArray alloc] init];
    
    UIView *datesChoicesView = [[UIView alloc] initWithFrame:CGRectMake((_screenWidth / 2) - (240 / 2), 0, 240, cellHeight)];
    datesChoicesView.backgroundColor = [UIColor whiteColor];
    datesChoicesView.layer.cornerRadius = 10;
    datesChoicesView.layer.borderColor = [UIColor grayColor].CGColor;
    datesChoicesView.layer.borderWidth = 1;
    [self addSubview:datesChoicesView];
    
    UILabel *datesChoicesLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, datesChoicesView.frame.size.width - 20, datesChoicesView.frame.size.height)];
    //datesChoicesLabel.backgroundColor = [UIColor greenColor];
    datesChoicesLabel.text = @"Multiple dates available";
    datesChoicesLabel.textAlignment = NSTextAlignmentCenter;
    datesChoicesLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    [datesChoicesView addSubview:datesChoicesLabel];
    
    UIButton *datesChoicesButton = [UIButton buttonWithType:UIButtonTypeSystem];
    datesChoicesButton.frame = CGRectMake(0, 0, datesChoicesView.frame.size.width, datesChoicesView.frame.size.height);
    //datesChoicesButton.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.2];
    [datesChoicesView addSubview:datesChoicesButton];
    
    [datesChoicesButton addTarget:self action:@selector(didShowDatesChoices) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *cellContainerView = [[UIView alloc] initWithFrame:CGRectMake(30, datesChoicesButton.frame.origin.y + datesChoicesButton.frame.size.height + 30, _screenWidth - 60, cellHeight * 4)];
    cellContainerView.backgroundColor = [UIColor whiteColor];
    cellContainerView.layer.borderWidth = 1;
    cellContainerView.layer.borderColor = [UIColor grayColor].CGColor;
    cellContainerView.layer.cornerRadius = 10;
    [self addSubview:cellContainerView];
    
    for (int i = 0; i < 4; i++) {
        
        UIImageView *iconDateImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, ((cellHeight / 2) - (_iconSize / 2)) + (i * cellHeight), _iconSize, _iconSize)];
        //iconDateImageView.backgroundColor = [UIColor greenColor];
        iconDateImageView.image = [UIImage imageNamed:[_iconArray objectAtIndex:i]];
        iconDateImageView.contentMode = UIViewContentModeScaleAspectFill;
        iconDateImageView.clipsToBounds = YES;
        [cellContainerView addSubview:iconDateImageView];
        
        UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(iconDateImageView.frame.origin.x + iconDateImageView.frame.size.width + 10, cellHeight * i, cellContainerView.frame.size.width - (iconDateImageView.frame.origin.x + iconDateImageView.frame.size.width + 20), cellHeight)];
        //detailLabel.backgroundColor = [UIColor orangeColor];
        detailLabel.text = @"Insert date";
        detailLabel.font = _font;
        //detailLabel.layer.borderWidth = 1;
        //detailLabel.layer.borderColor = [UIColor grayColor].CGColor;
        [cellContainerView addSubview:detailLabel];
        
        [labelMutableArray addObject:detailLabel];
        
        if(i >= 1){
            UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(0, cellHeight * i, cellContainerView.frame.size.width, 1)];
            seperator.backgroundColor = [UIColor lightGrayColor];
            [cellContainerView addSubview:seperator];
        }
    }
    
    _labelArray = [labelMutableArray copy];
    
    _bookButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _bookButton.frame = CGRectMake(cellContainerView.frame.origin.x, cellContainerView.frame.origin.y + cellContainerView.frame.size.height + 30, cellContainerView.frame.size.width, cellHeight);
    _bookButton.backgroundColor = [UIColor colorWithRed:21/255.0f green:115/255.0f blue:186/255.0f alpha:1.0];
    _bookButton.layer.cornerRadius = 10;
    [_bookButton setTitle:@"Book Now" forState:UIControlStateNormal];
    [_bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:_bookButton];
    
    [_bookButton addTarget:self action:@selector(paymentEvent) forControlEvents:UIControlEventTouchUpInside];
    
    UIWindow *appWindow = [UIApplication sharedApplication].keyWindow;
    
    self.frame = CGRectMake(0, _relativeView.frame.origin.y + _relativeView.frame.size.height + 30, appWindow.frame.size.width, _bookButton.frame.origin.y + _bookButton.frame.size.height + 50);
}

-(void)didShowDatesChoices{
    NSLog(@"show all dates");
    
    [_delegate didShowDatesChoices];
}

-(void)didSelectDate:(BatchesView*)batchesView{

}

-(void)paymentEvent{
    [_delegate paymentEvent:self];
}
@end
