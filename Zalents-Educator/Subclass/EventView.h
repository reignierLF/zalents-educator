//
//  EventView.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 23/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol EventViewDelegate

-(void)didSelectEvent:(UIButton*)sender;

@end

@interface EventView : UIView

@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *descriptionEvent;
@property (nonatomic, strong) NSString *price;

@property (nonatomic, weak) id <EventViewDelegate> delegate;

@end
