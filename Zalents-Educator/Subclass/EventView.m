//
//  EventView.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 23/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "EventView.h"
#import "ImageLoader.h"

@interface EventView ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIView *detailView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *priceLabel;

@end

@implementation EventView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, frame.size.width - 40, frame.size.height - 40)];
        //imageView.backgroundColor = [UIColor orangeColor];
        _imageView.layer.cornerRadius = 10;
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.image = [UIImage imageNamed:@"loading"];
        _imageView.clipsToBounds = YES;
        [self addSubview:_imageView];
        
        _detailView = [[UIView alloc] initWithFrame:CGRectMake(20, _imageView.frame.size.height - 30, _imageView.frame.size.width, 50)];
        _detailView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_detailView];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _detailView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){10.0, 10.0}].CGPath;
        _detailView.layer.mask = maskLayer;
        
//        self.layer.masksToBounds = NO;
//        self.layer.shadowOffset = CGSizeMake(-2, 2);
//        self.layer.shadowRadius = 5;
//        self.layer.shadowOpacity = 0.5;
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    ImageLoader *il = [[ImageLoader alloc] init];
    
    [il parseImage:_imageView url:_imageUrl];
    
    [_imageView setNeedsDisplay];
    [_detailView setNeedsDisplay];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, _detailView.frame.size.width - 80, _detailView.frame.size.height / 2)];
    //_titleLabel.backgroundColor = [UIColor greenColor];
    _titleLabel.text = _title;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
    _titleLabel.textColor = [UIColor darkGrayColor];
    [_detailView addSubview:_titleLabel];
    
    _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, (_detailView.frame.size.height / 2) - 3, _detailView.frame.size.width - 80, _detailView.frame.size.height / 2)];
    //_titleLabel.backgroundColor = [UIColor greenColor];
    _descriptionLabel.text = _descriptionEvent;
    _descriptionLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:10];
    _descriptionLabel.textColor = [UIColor darkGrayColor];
    [_detailView addSubview:_descriptionLabel];
    
    _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.size.width, 0, 70, _detailView.frame.size.height)];
    //_priceLabel.backgroundColor = [UIColor orangeColor];
    _priceLabel.text = [NSString stringWithFormat:@"%@ SGD",_price];
    _priceLabel.textAlignment = NSTextAlignmentRight;
    _priceLabel.textColor = [UIColor darkGrayColor];
    _priceLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:18];
    [_detailView addSubview:_priceLabel];
    
    UIButton *eventButton = [UIButton buttonWithType:UIButtonTypeCustom];
    eventButton.frame = CGRectMake(_imageView.frame.origin.x, _imageView.frame.origin.y, _imageView.frame.size.width, _imageView.frame.size.height);
    //eventButton.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
    eventButton.tag = self.tag;
    [self addSubview:eventButton];
    
    [eventButton addTarget:self action:@selector(didSelectEvent:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)didSelectEvent:(UIButton*)sender{
    
    [_delegate didSelectEvent:sender];
}
@end
