//
//  FeatureView.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 23/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeatureViewDelegate

-(void)didSelectFeature:(UIButton*)sender;

@end

@interface FeatureView : UIView

@property (nonatomic, strong) NSString *imageUrl;

@property (nonatomic, weak) id <FeatureViewDelegate> delegate;

@end
