//
//  FeatureView.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 23/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "FeatureView.h"

#import "ImageLoader.h"

@interface FeatureView ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation FeatureView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        //featureImageView.backgroundColor = [UIColor orangeColor];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.image = [UIImage imageNamed:@"loading"];
        _imageView.clipsToBounds = YES;
        [self addSubview:_imageView];
        
        /*
         UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, frame.size.height / 2, frame.size.width - 40, frame.size.height / 6)];
         //titleLabel.backgroundColor = [UIColor redColor];
         titleLabel.text = [NSString stringWithFormat:@"Feature %d",i];
         titleLabel.textColor = [UIColor whiteColor];
         titleLabel.numberOfLines = 0;
         titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
         [self addSubview:titleLabel];
         
         UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, titleLabel.frame.origin.y + titleLabel.frame.size.height, frame.size.width - 40, frame.size.height / 6)];
         //messageLabel.backgroundColor = [UIColor greenColor];
         messageLabel.text = [_wm.messages objectAtIndex:i];
         messageLabel.textColor = [UIColor whiteColor];
         messageLabel.numberOfLines = 0;
         messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
         [self addSubview:messageLabel];
         */
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    ImageLoader *il = [[ImageLoader alloc] init];
    
    [il parseImage:_imageView url:_imageUrl];
    
    [_imageView setNeedsDisplay];
    
    UIButton *featureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    featureButton.frame = CGRectMake(_imageView.frame.origin.x, _imageView.frame.origin.y, _imageView.frame.size.width, _imageView.frame.size.height);
    //featureButton.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
    featureButton.tag = self.tag;
    [self addSubview:featureButton];
    
    [featureButton addTarget:self action:@selector(didSelectFeature:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)didSelectFeature:(UIButton*)sender{
    
    [_delegate didSelectFeature:sender];
}
@end
