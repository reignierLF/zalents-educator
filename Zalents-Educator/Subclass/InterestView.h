//
//  InterestView.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 23/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterestView : UIView

@property (nonatomic, readwrite) NSArray *labelsArray;
@property (nonatomic, readwrite) NSArray *buttonsArray;

@property (nonatomic, readwrite) NSInteger startIndexAt;
@property (nonatomic, readwrite) NSInteger loop;

@end
