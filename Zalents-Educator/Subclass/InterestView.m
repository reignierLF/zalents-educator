//
//  InterestView.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 23/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "InterestView.h"

@implementation InterestView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
    
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    float xPos = 0;
    
    for (int i = 0; i < _loop; i++) {
        
        if(i != 0 && i <= _loop){
            
            xPos = [[_labelsArray objectAtIndex:(i + _startIndexAt) - 1] frame].size.width + [[_labelsArray objectAtIndex:(i + _startIndexAt) - 1] frame].origin.x + 10;

            NSLog(@"xPos = %f",xPos);
        }else{
            
            float tempXpos = 0;
            
            for (int j = 0; j < _loop; j++) {
                
                tempXpos += [[_labelsArray objectAtIndex:j + _startIndexAt] frame].size.width;
            }
            
            tempXpos += 20;
            
            xPos = self.frame.size.width - ((self.frame.size.width / 2) + (tempXpos / 2));
            NSLog(@"tempXpos = %f",tempXpos);
        }
        
        UILabel *label = [_labelsArray objectAtIndex:i + _startIndexAt];
        label.frame = CGRectMake(xPos, 0, label.frame.size.width, label.frame.size.height);
        [self addSubview:label];
        
        UIButton *button = [_buttonsArray objectAtIndex:i + _startIndexAt];
        button.frame = label.frame;
        [self addSubview:button];
    }
}

@end
