//
//  ListDatesView.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 28/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^result)(BOOL);

@protocol ListDatesViewDelegate

-(void)didSelectDate:(UIButton*)sender;

@end

@interface ListDatesView : UIView

@property (nonatomic, readwrite) NSArray *dateArray;
@property (nonatomic, readwrite) NSArray *startTimeArray;
@property (nonatomic, readwrite) NSArray *endTimeArray;

@property (nonatomic, weak) id <ListDatesViewDelegate> delegate;

-(void)animationHide:(result)complete;
-(void)animationShow:(result)complete;

@end
