//
//  ListDatesView.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 28/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ListDatesView.h"
#import "cUIScrollView.h"

@interface ListDatesView ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;
@property (nonatomic) float viewXPos;
@property (nonatomic) float viewYPos;

@end

@implementation ListDatesView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
        
        _viewXPos = frame.origin.x;
        _viewYPos = frame.origin.y;
        
        self.frame = CGRectMake(_viewXPos, -(_viewYPos + _viewHeight), _viewWidth, _viewHeight);
        self.layer.cornerRadius = 10;
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, 40)];
        //titleLabel.backgroundColor = [UIColor greenColor];
        titleLabel.text = @"Multiple dates available";
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:14];
        [self addSubview:titleLabel];
        
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    float height = 40;
    
    cUIScrollView *scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, height, _viewWidth, _viewHeight - height)];
    //scrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, height * _dateArray.count);
    scrollView.delaysContentTouches = NO;
    [self addSubview:scrollView];
    
    for (int i = 0; i < _dateArray.count; i++) {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, height * i, _viewWidth- 20, height)];
        //titleLabel.backgroundColor = [UIColor orangeColor];
        titleLabel.text = [NSString stringWithFormat:@"%@, %@ - %@",[_dateArray objectAtIndex:0],[_startTimeArray objectAtIndex:0], [_endTimeArray objectAtIndex:0]];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:12];
        [scrollView addSubview:titleLabel];
        
        UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(20, titleLabel.frame.size.height - 1, titleLabel.frame.size.width - height, 1)];
        seperator.backgroundColor = [UIColor grayColor];
        [titleLabel addSubview:seperator];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        //button.backgroundColor = [UIColor purpleColor];
        button.frame = CGRectMake(0, height * i, scrollView.frame.size.width, height);
        button.tag = i;
        [scrollView addSubview:button];
        
        [button addTarget:self action:@selector(didSelectDate:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    //scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, height * _dateArray.count);

    NSLog(@"date = %@",_dateArray);
    NSLog(@"startTime = %@", _startTimeArray);
    NSLog(@"endTime = %@",_endTimeArray);
}

-(void)animationHide:(result)complete{
    
    [UIView transitionWithView:self
                      duration:0.3f
                       options:UIViewAnimationOptionCurveEaseInOut
                    animations:^{
                        self.frame = CGRectMake(_viewXPos, -(_viewYPos + _viewHeight), _viewWidth, _viewHeight);
                    }completion:^(BOOL done){
                        complete(YES);
                        //[_delegate animationHide];
                    }];
}

-(void)animationShow:(result)complete{
    
    [UIView transitionWithView:self
                      duration:0.3f
                       options:UIViewAnimationOptionCurveEaseInOut
                    animations:^{
                        self.frame = CGRectMake(_viewXPos, _viewYPos, _viewWidth, _viewHeight);
                    }completion:^(BOOL done){
                        complete(YES);
                        //[_delegate animationShow];
                    }];
}

-(void)didSelectDate:(UIButton*)sender{
    
    [_delegate didSelectDate:sender];
}
@end
