//
//  ListEventView.h
//  Zalents-Educator
//
//  Created by Livefitter on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ListEventViewDelegate

-(void)didSelectListEvent:(UIButton *)sender;

@end

@interface ListEventView : UIView

@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *address;

@property (nonatomic, weak) id <ListEventViewDelegate> delegate;

@end
