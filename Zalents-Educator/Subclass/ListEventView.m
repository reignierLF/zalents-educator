//
//  ListEventView.m
//  Zalents-Educator
//
//  Created by Livefitter on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ListEventView.h"
#import "ImageLoader.h"

@interface  ListEventView ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIView *detailView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *addressLabel;

@end

@implementation ListEventView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, frame.size.width - 40, frame.size.height - 70 )];
        //imageView.backgroundColor = [UIColor orangeColor];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
        
        CAShapeLayer * maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _imageView.bounds byRoundingCorners: UIRectCornerAllCorners cornerRadii: (CGSize){10.0, 10.}].CGPath;
        
        _imageView.layer.mask = maskLayer;
        
        [self addSubview:_imageView];
        
        _detailView = [[UIView alloc] initWithFrame:CGRectMake(20, _imageView.frame.size.height - 60, _imageView.frame.size.width, 70)];
        
        _detailView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
        
        CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
        maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: _detailView.bounds byRoundingCorners: UIRectCornerBottomLeft|UIRectCornerBottomRight cornerRadii: (CGSize){10.0, 10.}].CGPath;
        
        _detailView.layer.mask = maskLayer2;
        
        [self addSubview:_detailView];
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    ImageLoader *il = [[ImageLoader alloc] init];
    
    [il parseImage:_imageView url:_imageUrl];
    
    [_imageView setNeedsDisplay];
    [_detailView setNeedsDisplay];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, _detailView.frame.size.width - 20, 25)];
    //_titleLabel.backgroundColor = [UIColor greenColor];
    _titleLabel.text = _title;
    _titleLabel.textColor = [UIColor blackColor];
//    [_titleLabel setFont:[UIFont fontWithName:@"fontname" size:128.0]];
    
    [_detailView addSubview:_titleLabel];
    
    _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, _titleLabel.frame.size.height +3, _detailView.frame.size.width - 10, 20)];
    //_priceLabel.backgroundColor = [UIColor orangeColor];
    _dateLabel.text = _date;
    _dateLabel.textColor = [UIColor blackColor];
    [_dateLabel setFont:[UIFont systemFontOfSize:15]];
//    [_dateLabel setFont:[UIFont fontWithName:@"fontname" size:128.0]];
    [_detailView addSubview:_dateLabel];
    
    _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, _dateLabel.frame.size.height + _titleLabel.frame.size.height +3, _detailView.frame.size.width - 10, 20)];
    //_priceLabel.backgroundColor = [UIColor orangeColor];
    _addressLabel.text = _address;
    _addressLabel.textColor = [UIColor blackColor];
    [_addressLabel setFont:[UIFont systemFontOfSize:15]];
//    [_addressLabel setFont:[UIFont fontWithName:@"fontname" size:128.0]];
    [_detailView addSubview:_addressLabel];
    
    UIButton *eventButton = [UIButton buttonWithType:UIButtonTypeCustom];
    eventButton.frame = CGRectMake(_imageView.frame.origin.x, _imageView.frame.origin.y, _imageView.frame.size.width, _imageView.frame.size.height);
    //eventButton.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
    eventButton.tag = self.tag;
    [self addSubview:eventButton];
    
    [eventButton addTarget:self action:@selector(didSelectListEvent:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)didSelectListEvent:(UIButton*)sender{
    
    [_delegate didSelectListEvent:sender];
}
@end



