//
//  PastEvents.h
//  Zalents-Educator
//
//  Created by Livefitter on 29/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PastEvents : NSObject

@property (nonatomic, strong) NSString *pastEventsId;
@property (nonatomic, strong) NSString *pastEventsuser;

@end
