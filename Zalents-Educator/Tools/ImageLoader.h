//
//  ImageLoader.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageLoader : NSObject

-(void)parseImage:(UIImageView*)imageView url:(NSString*)urlString;

@end
