//
//  ImageLoader.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ImageLoader.h"

@interface ImageLoader ()

@end

@implementation ImageLoader

-(instancetype)init{
    
    self = [super init];
    
    if(self){
        
    }
    
    return self;
}

-(void)parseImage:(UIImageView*)imageView url:(NSString*)urlString{
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        /*
         * Check if image url is null
         * If null = image is replace with no image background picture
         * If not null = image is loaded from url
         */
        
        __block UIImage *image;
        
        if ([urlString isEqualToString:@"notFound"]) {
            
            image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",urlString]];
        }else{
            
            NSURL *url = [NSURL URLWithString:urlString];
            
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:urlString];
            
            if(data == nil){
                data = [NSData dataWithContentsOfURL:url];
                
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:urlString];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"no image cache but make cache now");
            }else{
                NSLog(@"image cache");
            }
            
            /*
             * Incase failed to finish the downloading the image
             * We replace the image to our no image background picture
             */
            
            if(data == nil){
                image = [UIImage imageNamed:@"notFound"];
            }else{
                image = [UIImage imageWithData:data];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            /*
             * Set new image
             */
            
            UIImage * newImage = image;
            
            [UIView transitionWithView:imageView
                              duration:1.0f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                imageView.image = newImage;
                            }completion:nil];
        });
    });
}
@end
