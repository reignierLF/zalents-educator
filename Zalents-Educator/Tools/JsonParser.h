//
//  JsonParser.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 1/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * Used for block, get result after
 * response from the server api
 */

typedef void(^result)(BOOL);

@interface JsonParser : NSObject

@property (nonatomic, readonly) NSURLRequest *request;
@property (nonatomic, readonly) NSURLResponse *response;
@property (nonatomic, readonly) NSInteger statusCode;
@property (nonatomic, readonly) NSUInteger fileSize;
@property (nonatomic, readonly) NSData *data;
@property (nonatomic, readonly) NSDictionary *jsonDictionary;
@property (nonatomic, readonly) NSArray *jsonArray;
@property (nonatomic, readonly) NSError *error;

/*
 * Required URL
 */

-(void)parseJsonURL:(NSString*)urlString parsing:(result)complete;
-(void)byJson:(NSString*)method url:(NSString*)urlString param:(NSDictionary*)parameters onCompletion:(result)complete;
-(void)byFormData:(NSString*)method url:(NSString*)urlString param:(NSArray*)parameters onCompletion:(result)complete;
-(void)byXML:(NSString*)method url:(NSString*)urlString param:(NSArray*)parameters onCompletion:(result)complete;
@end
