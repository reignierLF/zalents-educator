//
//  JsonParser.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 1/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "JsonParser.h"

@implementation JsonParser

-(void)parseJsonURL:(NSString*)urlString parsing:(result)complete{
    
    /*
     * use this to display "Network indicator"
     */
    
    //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    _request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];

    [NSURLConnection sendAsynchronousRequest:_request
                     queue:[NSOperationQueue mainQueue]
                     completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                         
                         /*
                          * complete(BOOL:Parameter) = is a function block
                          *
                          * complete(NO or false) = no json results even there are no server error
                          *
                          * complete(YES or true) = with json results use "jsonDictionary" to see the results
                          */
                         
                         _response = response;
                         
                         /*
                          * Get status code visit https://www.w3.org/Protocols/HTTP/HTRESP.html
                          * for status code details
                          */
                         
                         [self getStatusCode:response];
                         
                         if(_statusCode > 500 || _statusCode > 400){
                             
                             /*
                              * SERVER ERRORS
                              */
                             
                             switch(_statusCode){
                                 case 500 :
                                     NSLog(@"Status code : %ld (Internal Error)",(long)_statusCode);
                                     break;
                                 case 501 :
                                     NSLog(@"Status code : %ld (Not implemented)",(long)_statusCode);
                                     break;
                                 case 502 :
                                     NSLog(@"Status code : %ld (Service temporarily overloaded)",(long)_statusCode);
                                     break;
                                 case 503 :
                                     NSLog(@"Status code : %ld (Gateway timeout)",(long)_statusCode);
                                     break;
                                 default :
                                     NSLog(@"Status code : %ld",(long)_statusCode);
                             }
                             
                             complete(NO);
                         }else{
                             
                             /*
                              * Size of the file in bytes
                              */
                             
                             _fileSize = data.length;
                             
                             if(_fileSize == 0){
                                 
                                 /*
                                  * File is size is 0 bytes
                                  * do something here if nessessary
                                  */
                                 
                             }else{
                                 
                                 /*
                                  * Raw data that display in hex
                                  */
                                 
                                 _data = data;
                                 
                                 /*
                                  * Serializing data to readable json
                                  */
                                 
                                 _jsonDictionary = [NSJSONSerialization
                                                    JSONObjectWithData: data
                                                               options: 0
                                                                 error: nil];
                                 _jsonArray = [NSJSONSerialization
                                               JSONObjectWithData: data
                                               options: 0
                                               error: nil];
                                 
                                 if(_jsonDictionary == nil || _jsonArray == nil){
                                     
                                     /*
                                      * It is either wrong URL or the json is just plain empty
                                      */
                                     
                                     NSLog(@"No Json data parsed");
                                     complete(NO);
                                 }else{
                                     
                                     /*
                                      * Complete process with no errors
                                      */
                                     
                                     complete(YES);
                                 }
                             }
                         }
                         
                         /*
                          * Old code, comment out just reference
                          *
                          
                         if(_statusCode == 500){
                             //SERVER ERROR
                             NSLog(@"Server Error : 500 (Internal Error)");
                             complete(NO);
                         }else if(_statusCode == 501){
                             NSLog(@"Server Error : 501 (Not implemented)");
                             complete(NO);
                         }else if(_statusCode == 502){
                             NSLog(@"Server Error : 502 (Service temporarily overloaded)");
                             complete(NO);
                         }else if(_statusCode == 503){
                             NSLog(@"Server Error : 503 (Gateway timeout)");
                             complete(NO);
                         }else{
                             _fileSize = data.length;
                             
                             if(_fileSize == 0){
                                 //FILE SIZE ZERO
                             }else{
                                 //JSON RAW DATA
                                 _data = data;
                                 
                                 //JSON PARSED TO DICTIONARY
                                 _jsonDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                  options:0
                                                                                    error:nil];
                                 
                                 complete(YES);
                             }
                         }
                        */
    }];
}

-(void)getStatusCode:(NSURLResponse*)response{
    
    /*
     * Method to get status code
     */
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    _statusCode = httpResponse.statusCode;
    NSLog(@"header field = %@",httpResponse.allHeaderFields);
    NSLog(@"suggested file name = %@",httpResponse.suggestedFilename);
    NSLog(@"text encoding name = %@",httpResponse.textEncodingName);
}

-(void)postJson:(NSString*)url param:(NSArray*)dictionary{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
    if (jsonData) {
        NSLog(@"jsonData = %@", jsonData);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        if (!request) NSLog(@"Error creating the URL Request");
        //[request setHTTPMethod:@"POST"];
        //[request setHTTPBody:jsonData];
        //[request setValue:@"text/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPMethod:@"POST"];
        //set request content type we MUST set this value.
        //[request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        
        //set post data of request
        //[request setHTTPBody:[dataString dataUsingEncoding:NSUTF8StringEncoding]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //[request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        //[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:jsonData];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                                   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                   
                                   NSLog(@"error = %@", connectionError);
                                   
                                   NSLog(@"status code = %ld",(long)httpResponse.statusCode);
                                   NSLog(@"status url = %@",httpResponse.URL);
                                   NSLog(@"status header = %@", httpResponse.allHeaderFields);
                                   NSLog(@"status text encoding = %@", httpResponse.textEncodingName);
                                   NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   NSLog(@"response : %@", string);
                               }];
        
        /*
        // Send a synchronous request
        NSError *error;
        NSURLResponse *response;
        //NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSLog(@"responseData = %lu", (unsigned long)responseData.length);
        NSString *string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        */
        
        //NSLog(@"response : %@", string);
    } else {
        NSLog(@"Unable to serialize the data %@: %@", dictionary, error);
    }
}

-(void)byJson:(NSString*)method url:(NSString*)urlString param:(NSDictionary*)parameters onCompletion:(result)complete{
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"a3510943-59a1-ed2f-722a-8e4de84e49db" };
    /*
    NSDictionary *parameters = @{ @"user_event[credit_group_id]": @"8",
                                  @"user_event[event_batch_id]": @"60",
                                  @"user_event[to_obtain_approval]": @"true" };
    */
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:method];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        _error = error;
                                                        NSLog(@"%@", error);
                                                        
                                                        complete(NO);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        _response = response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        complete(YES);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)byFormData:(NSString*)method url:(NSString*)urlString param:(NSArray*)parameters onCompletion:(result)complete{
    /*
     * Solutions to Andy's weird POST because he is not
     * receiving json and no headers being specify
     * instead he accept straight from body with form-data
     *
     * Generate by Postman in Google extensions
     * use this extension from google incase of emergency
     */
    NSDictionary *headers = @{ @"content-type": @"multipart/form-data; boundary=---011000010111000001101001",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"d457dad9-0534-db32-b6c0-d5e0b9de3bfe" };
    
    /*
    NSArray *parameters = @[ @{ @"name": @"user_event[event_batch_id]", @"value": @"59" },
                             @{ @"name": @"user_event[to_obtain_approval]", @"value": @"true" },
                             @{ @"name": @"user_event[credit_group_id]", @"value": @"1" } ];
     */
    NSString *boundary = @"---011000010111000001101001";
    
    NSError *error;
    NSMutableString *body = [NSMutableString string];
    
    for (NSDictionary *param in parameters) {
        
        [body appendFormat:@"--%@\r\n", boundary];
        
        if (param[@"fileName"]) {
            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"; filename=\"%@\"\r\n", param[@"name"], param[@"fileName"]];
            [body appendFormat:@"Content-Type: %@\r\n\r\n", param[@"contentType"]];
            [body appendFormat:@"%@", [NSString stringWithContentsOfFile:param[@"fileName"] encoding:NSUTF8StringEncoding error:&error]];
            if (error) {
                NSLog(@"%@", error);
            }
        } else {
            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"\r\n\r\n", param[@"name"]];
            [body appendFormat:@"%@", param[@"value"]];
        }
    }
    
    [body appendFormat:@"\r\n--%@--\r\n", boundary];
    
    NSData *postData = [body dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:method];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    _jsonDictionary = [NSJSONSerialization
                                                                       JSONObjectWithData: data
                                                                       options: 0
                                                                       error: nil];
                                                    
                                                    _jsonArray = [NSJSONSerialization
                                                                  JSONObjectWithData: data
                                                                  options: 0
                                                                  error: nil];
                                                    
                                                    if (error) {
                                                        _error = error;
                                                        NSLog(@"Error : %@", error);
                                                        
                                                        complete(NO);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        _response = httpResponse;
                                                        NSLog(@"Response : %@", httpResponse);
                                                        
                                                        complete(YES);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)byXML:(NSString*)method url:(NSString*)urlString param:(NSArray*)parameters onCompletion:(result)complete{
    NSDictionary *headers = @{ @"content-type": @"application/xml",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"ae625b87-2f26-7815-b386-0150cb3a1bdf" };
    
    NSString *xmlString;
    
    if(parameters.count == 2){
        NSLog(@"booking at uob summit");
        xmlString = [NSString stringWithFormat:@"<user_event><event_batch_id>%@</event_batch_id><to_obtain_approval>%@</to_obtain_approval></user_event>",[parameters objectAtIndex:0],[parameters objectAtIndex:1]];
    }else if(parameters.count == 3){
        NSLog(@"booking at skillsfuture");
        xmlString = [NSString stringWithFormat:@"<user_event><credit_group_id>%@</credit_group_id><event_batch_id>%@</event_batch_id><to_obtain_approval>%@</to_obtain_approval></user_event>",[parameters objectAtIndex:0],[parameters objectAtIndex:1],[parameters objectAtIndex:2]];
    }
    
    NSData *postData = [[NSData alloc] initWithData:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:method];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    _jsonDictionary = [NSJSONSerialization
                                                                       JSONObjectWithData: data
                                                                       options: 0
                                                                       error: nil];
                                                    
                                                    _jsonArray = [NSJSONSerialization
                                                                  JSONObjectWithData: data
                                                                  options: 0
                                                                  error: nil];
                                                    
                                                    if (error) {
                                                        _error = error;
                                                        NSLog(@"Error : %@", error);
                                                        
                                                        complete(NO);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        _response = httpResponse;
                                                        NSLog(@"Response : %@", httpResponse);
                                                        
                                                        complete(YES);
                                                    }
                                                }];
    [dataTask resume];
}
@end
