//
//  MenuController.h
//  Zalents
//
//  Created by LF-Mac-Air on 23/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Menu.h"

/*
 * Delegate / Protocol
 */

@class MenuController;
@protocol MenuControllerDelegate

- (void)didSelectMenu:(MenuController*)menuController viewController:(UIViewController*)viewController;
- (void)didSelectSubMenu:(MenuController*)menuController;

@end

typedef void(^Block)(void);

@interface MenuController : UIView{
    Block block;
}

@property (nonatomic, readonly) NSArray *matrixToList;
@property (nonatomic, readonly) UIViewController *currentViewController;
@property (nonatomic) BOOL popToFront;
@property (nonatomic) float headerSpace;

@property (nonatomic, strong) Menu *menu;

@property (nonatomic, weak) id <MenuControllerDelegate> delegate;

-(void)initNavigationBarItems;

-(void)toogleMenuView;

/*
 *
 */

@property (nonatomic, copy) Block menuViewDisappear;

@end
