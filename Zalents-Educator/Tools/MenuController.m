//
//  MenuController.m
//  Zalents
//
//  Created by LF-Mac-Air on 23/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "MenuController.h"
#import "cUIScrollView.h"

@interface MenuController ()

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic) float cellHeight;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *menuBackgroundColor;
@property (nonatomic, strong) UIColor *subMenuBackgroundColor;
@property (nonatomic, strong) UIColor *linebreakColor;

@property (nonatomic, strong) UIViewController *vc;
@property (nonatomic) BOOL toogleMenu;
@property (nonatomic) float alphaFade;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) cUIScrollView *scrollView;
@property (nonatomic, strong) UIButton *backViewButton;

@property (nonatomic, strong) NSMutableArray *semiMatrixArray;
@property (nonatomic, strong) NSMutableArray *cellContainerArray;
@property (nonatomic, strong) NSMutableArray *cellArrowArray;
@property (nonatomic, strong) NSMutableArray *toogleArray;

@property (nonatomic, readwrite) float collapseSpeed;

@end

@implementation MenuController

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        
        _menu = [[Menu alloc] init];
        
        /*
         * Vars for frame size
         */
        
        _screenWidth = frame.size.width;
        _screenHeight = frame.size.height;
        
    }
    
    return self;
}

/*
 * Build the entire menu controller with this method
 */

-(void)constructMenuController{
    
    /*
     * Load a custom settings for menu buttons and sub-menu buttons
     *
     * Requires all value form the Menu class (Menu.h/Menu.m)
     *
     * Note : Event without the Menu class, the app will still
     *        run but will not able to see the menu
     */
    
    [self loadSettings];
    
    /*
     * At start the menu is invisible
     */
    
    self.alpha = 0.0;
    
    /*
     * This will detect the current view controller present
     */
    
    [self getCurrentViewControllerPresented];
    
    /*
     * Fake background of menu and mask of current view controller
     */
    
    [self maskCurrentViewControllerPresented];
    
    /*
     * Init the menu bar button for top left of the screen
     */
    
    [self initNavigationBarItems];
    
    /*
     * semiMatrixArray : This is where we store all titles that
     *                   we just trimmed and removing the those
     *                   without title in the matrix
     */
    
    _semiMatrixArray = [[NSMutableArray alloc] init];
    
    /*
     * cellContainerArray : This is where we store all the menu buttons
     *                      and sub-menu buttons
     */
    
    _cellContainerArray = [[NSMutableArray alloc] init];
    
    /*
     * cellArrowArray : This is where we store all the arrows for
     *                  those menu buttons with sub-menu buttons
     */
    
    _cellArrowArray = [[NSMutableArray alloc] init];
    
    /*
     * toogleArray : This is where we store all the bool value for
     *               those menu buttons with sub-menu buttons
     */
    
    _toogleArray = [[NSMutableArray alloc] init];
    
    _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    _headerView.backgroundColor = _menuBackgroundColor;
    [self addSubview:_headerView];
    
    /*
     * Main container for all menu buttons and sub-menu buttons
     */
    
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, _headerSpace, _screenWidth, _screenHeight)];
    //_scrollView.backgroundColor = [UIColor greenColor];
    _scrollView.backgroundColor = _menuBackgroundColor;
    _scrollView.contentSize = CGSizeMake(_screenWidth, _screenHeight);
    [self addSubview:_scrollView];
    
    /*
     * This is where we store all the titles and nodes from the matrix
     */
    
    NSMutableArray *matrixToList = [[NSMutableArray alloc] init];
    
    /*
     * Loop base on each row in the matrix
     */
    
    for (int i = 0; i < _menu.list.count; i++) {
        
        /*
         * Get the array from each row from the matrix
         */
        
        NSArray *rowArray = [_menu.list objectAtIndex:i];
        
        /*
         * This is where we store all the title(s) from each row
         *
         * Note : if that row has a single title then it will turn
         *        to menu button but if that row has multiple titles
         *        then we will make that menu button with sub-menu buttons
         */
        
        NSMutableArray *newRowArray = [[NSMutableArray alloc] init];
        
        /*
         * Loop base on section/column size of matrix
         */
        
        for (int j = 0; j < rowArray.count; j++) {
            
            /*
             * Get title for each nodes in the matrix
             */
            
            NSString *title = [rowArray objectAtIndex:j];
            
            /*
             * If on that node doesnt have a title, we are removing that node
             */
            
            if(title.length != 0){
                
                /*
                 * If that node has title, we store it on this
                 * array(matrixToList) for refernce
                 */
                
                [matrixToList addObject:[NSString stringWithFormat:@"[%d][%d]%@",i,j,title]];
                
                /*
                 * Then we are storing them to new row array
                 * because we just removed all nodes that
                 * doesnt have a title
                 */
                
                [newRowArray addObject:title];
            }
        }
        
        /*
         * After trimming all non-title nodes
         * we store them to new 2d-array
         */
        
        [_semiMatrixArray addObject:newRowArray];
        
        /*
         * For each row, we are assigning them with toggle bool value
         */
        
        [_toogleArray addObject:[NSNumber numberWithBool:NO]];
    }
    
    //NSLog(@"_semiMatrixArray = %@",_semiMatrixArray);
    
    /*
     * For refrence use, see all titles and their
     * corresponding nodes
     */
    
    _matrixToList = matrixToList;
    
    /*
     * Loop base on the each row in 2d-array
     */
    
    for (int i = 0; i < _semiMatrixArray.count; i++) {
        
        /*
         * Get each row from the 2d-array
         */
        
        NSArray *rowArray = [_semiMatrixArray objectAtIndex:i];
        
        /*
         * If that row has only 1 title, then we will turn
         * it to single menu button
         *
         * Else
         *
         * The row has a multiple titles, then we will turn
         * it to menu button with sub-menu buttons
         */
        
        if(rowArray.count == 1){
            
            /*
             * Layout for each menu button
             */
            
            [self initMenuButtonCell:[NSString stringWithFormat:@"%@", [rowArray objectAtIndex:0]] row:i];
            //NSLog(@"single menu button");
        }else{
            
            /*
             * Layout for each menu button and sub-menu button
             */
            
            [self initSubMenuButtonCell:rowArray row:i];
            //NSLog(@"menu button with sub-menu button");
        }
    }
    
    /*
     * Get the last menu button from the array, we need
     * it for excess height of the content size of scrollview
     */
    
    UIView *lastCellContainerView = [_cellContainerArray lastObject];
    
    /*
     * We are calculating the height of content size of scrollview
     */
    
    float excessViewHeight = fabs((_cellHeight * _cellContainerArray.count) - _scrollView.contentSize.height);
    
    /*
     * We are masking the excess height of the content size in the scrollview
     * so doesnt show the sub-menu of the last menu-button in the array
     */
    
    UIView *excessView = [[UIView alloc] initWithFrame:CGRectMake(0, lastCellContainerView.frame.origin.y + _cellHeight, _screenWidth, excessViewHeight)];
    excessView.backgroundColor = _menuBackgroundColor;
    [_scrollView addSubview:excessView];
    
    /*
     * Adding the excess view in this array so it
     * is include in the animation
     */
    
    [_cellContainerArray addObject:excessView];
    
    //NSLog(@"_cellContainerArray = %@", _cellContainerArray);
}

-(void)didMoveToSuperview{
    
    /*
     * Check if we want our menu controller above all
     * views even over top of navigation controller
     *
     * IF true, bring the menu controller in front when called
     * ELSE stay the menu controller where it was
     */
    
    if(_popToFront){
        
        /*
         * Get the main window
         */
        
        UIWindow *appWindow = [UIApplication sharedApplication].keyWindow;
        
        /*
         * We change the menu controller height base on screen height
         */
        
        _screenHeight = appWindow.frame.size.height;
        
        /*
         * Change the height of scrollview base on screen height
         */
        
        _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, appWindow.frame.size.height);
        
        /*
         * Add the menu controller on main window instead on current view
         */
        
        [appWindow addSubview:self];
    }
    
    [self constructMenuController];
}

-(void)getCurrentViewControllerPresented{
    
    /*
     * Since our root viewcontroller is navigationController
     * and was set in appDelegate class, we need to get the current
     * viewcontroller displayed in the screen.
     *
     * By browsing all the child in navigationController
     * and get the last viewcontroller
     */
    
    _vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    _vc = [_vc.childViewControllers lastObject];
    
    _currentViewController = _vc;
}

-(void)maskCurrentViewControllerPresented{
    
    /*
     * Act as mask for current view controller and
     * background for menu, whether the user click on
     * empty field right-side of menu, it will hide the menu
     */
    
    _backViewButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _backViewButton.frame = CGRectMake(_scrollView.frame.origin.x + _scrollView.frame.size.width, 0, 0, _screenHeight);
    _backViewButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    _backViewButton.tag = -1;
    [self addSubview:_backViewButton];
    
    [_backViewButton addTarget:self action:@selector(toogleMenuView) forControlEvents:UIControlEventTouchUpInside];
}

-(void)initNavigationBarItems{
    
    /*
     * This create a logo in the navigation bar and align at center
     */
    
    UIImage *logoImage = [UIImage imageNamed:@"zalents logo big"];
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logoImage];
    logoImageView.frame = CGRectMake(0, 0, _screenWidth / 2, 30);
    //logoImageView.backgroundColor = [UIColor greenColor];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    /*
     * We are replacing or adding a image at title view
     * navigation bar
     */
    
    _vc.navigationItem.titleView = logoImageView;

    /*
     * This create a custom button with image without pixilating the image
     */
    
    UIButton *menuButton = [[UIButton alloc] init];
    menuButton.frame = CGRectMake(0, 0, _vc.navigationController.navigationBar.frame.size.height / 2, _vc.navigationController.navigationBar.frame.size.height / 2);
    //menuButton.backgroundColor = [UIColor orangeColor];
    [menuButton setImage:[UIImage imageNamed:@"menu"]forState:UIControlStateNormal];
    menuButton.contentMode = UIViewContentModeScaleAspectFill;
    [menuButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    //menuButton.clipsToBounds = YES;
    [menuButton addTarget:self action:@selector(toogleMenuView) forControlEvents:UIControlEventTouchUpInside];
    
    /*
     * Add Menu button in navigation bar at left of the screen
     * and replacing the back button
     */
    
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    _vc.navigationItem.leftBarButtonItem = menuBarButton;
    
    /*
     * Same as the menu bar but on the opposite side which is
     * in the top-right corner on the screen
     *
     * NOTE : Currently disable but later will add function to
     *        enable and disable this feature button
     */
    
    /*
    UIButton *extraButton = [[UIButton alloc] init];
    extraButton.frame = CGRectMake(0, 0, _vc.navigationController.navigationBar.frame.size.height / 2, _vc.navigationController.navigationBar.frame.size.height / 2);
    //menuButton.backgroundColor = [UIColor orangeColor];
    [extraButton setImage:[UIImage imageNamed:@"s_calendar"]forState:UIControlStateNormal];
    extraButton.contentMode = UIViewContentModeScaleAspectFill;
    [extraButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    //extraButton.clipsToBounds = YES;
    [extraButton addTarget:self action:@selector(extraButtonEvent) forControlEvents:UIControlEventTouchUpInside];
    */
    
    UIView *planeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _vc.navigationController.navigationBar.frame.size.height / 2, _vc.navigationController.navigationBar.frame.size.height / 2)];
    planeView.backgroundColor = [UIColor clearColor];
    
    /*
     * Add extra button in navigation bar at right of the screen
     */
    
    UIBarButtonItem *extraBarButton = [[UIBarButtonItem alloc] initWithCustomView:planeView];
    _vc.navigationItem.rightBarButtonItem = extraBarButton;
}

-(void)toogleMenuView{

    /*
     * Start always to 0 to avoid warning initialization
     */
    
    float menuXPosition = 0.0;
    
    float backButtonViewWidth = 0.0;
    
    /*
     * Toogle condition for top-right menu button or
     * in the navigation bar
     */
    
    if(!_toogleMenu){
        
        /*
         * If menu is not yet shown or not in the screen
         * then set the toogle menu value to true
         */
        
        _toogleMenu = YES;
        
        /*
         * Set view to visible
         */
        
        _alphaFade = 1.0;
        
        /*
         * Set to 0 the x-axis position of menu
         * because our current menu x-axis position is off
         * to left screen/window or the device
         */
        
        menuXPosition = 0;
        
        /*
         * Set the width of fake background of menu to current
         * size of the view controlller window
         */
        
        backButtonViewWidth = _vc.view.frame.size.width;
        
        /*
         * Change the size of the current
         */
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, _screenWidth * 2, self.frame.size.height);
        
        //NSLog(@"show menu = %i", _toogleMenu);
    }else if(_toogleMenu){
        
        /*
         * If menu is on the screen then set 
         * the toogle menu value to false
         */
        
        _toogleMenu = NO;
        
        /*
         * Set view to invisible
         */
        
        _alphaFade = 0.0;
        
        /*
         * Set to negative x-axis position of menu
         * base on its width
         */
        
        menuXPosition = -_screenWidth;
        
        /*
         * Set the width of fake background back to 0
         */
        
        backButtonViewWidth = 0;
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, _screenWidth, self.frame.size.height);
        
        //NSLog(@"hide menu = %i", _toogleMenu);
    }
    
    /*
     * Since the width size of this menu is limited we are
     * extending the width of fake background to the size of
     * window or the device wherever the menu shows up and
     * resize back to original state when menu hides
     */
    
    _backViewButton.frame = CGRectMake(_scrollView.frame.origin.x + _scrollView.frame.size.width, 0, backButtonViewWidth, _screenHeight);
    
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         /*
                          * Show and hide animation of menu
                          */
                         
                         self.alpha = _alphaFade;
                         
                         self.frame = CGRectMake(menuXPosition, self.frame.origin.y, self.frame
                                                 .size.width, self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                             /*
                              * Add something here after animation
                              */
                             
                             if(self.alpha == 0.0){
                                 NSLog(@"done animating");
                                 
                                 /*
                                  * Check if this function is exist
                                  *
                                  * IF true, call this function block and set whatever todo
                                  * in our case we are setting the this method
                                  * [self didSelectMenu: viewController:];
                                  */
                                 
                                 if (_menuViewDisappear != nil) {
                                     _menuViewDisappear();
                                 }
                             }
                         }
                     }];
}

-(void)extraButtonEvent{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Zalents Educator" message:@"Mmmmh.. iPhone 7?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:close];
    
    [_vc presentViewController:alertController animated:YES completion:nil];
}

/*
 * Layout UI of the menu buttons
 * 
 * Note : Too lazy to make comment for this one
 *        but it is obviously readable
 */

-(void)initMenuButtonCell:(NSString*)param row:(NSInteger)row{
    
    UIView *cellContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, _cellHeight * row, _screenWidth, _cellHeight)];
    //cellContainerView.backgroundColor = [UIColor orangeColor];
    cellContainerView.backgroundColor = _menuBackgroundColor;
    [_scrollView addSubview:cellContainerView];
    
    float logoAdjustment = 0.0;
    
    UIImage *logo_img = [UIImage imageNamed:[NSString stringWithFormat:@"%@", [_menu.logo objectAtIndex:row]]];
    
    if(logo_img == nil){
        
        logoAdjustment = 10;
    }else{
        
        logoAdjustment = _cellHeight + 30;
    }
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logo_img];
    logoImageView.frame = CGRectMake(20, 25 / 2, _cellHeight - 25, _cellHeight - 25);
    //logoImageView.backgroundColor = [UIColor orangeColor];
    logoImageView.contentMode = UIViewContentModeScaleAspectFill;
    logoImageView.clipsToBounds = YES;
    [cellContainerView addSubview:logoImageView];
    
    UILabel *cellText = [[UILabel alloc] initWithFrame:CGRectMake(logoAdjustment, 0, _screenWidth - logoAdjustment, _cellHeight)];
    cellText.text = param;
    cellText.textColor = _textColor;
    //cellText.backgroundColor = [UIColor cyanColor];
    cellText.backgroundColor = _menuBackgroundColor;
    cellText.font = _font;
    [cellContainerView addSubview:cellText];
    
    UIButton *cellButton = [UIButton buttonWithType:UIButtonTypeSystem];
    cellButton.frame = CGRectMake(0, 0, _screenWidth, _cellHeight);
    cellButton.tag = row;
    [cellContainerView addSubview:cellButton];
    
    [cellButton addTarget:self action:@selector(cellButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonLineBreakView = [[UIView alloc] initWithFrame:CGRectMake(10, cellButton.frame.size.height - 1, cellButton.frame.size.width - 20, 1)];
    buttonLineBreakView.backgroundColor = _linebreakColor;
    [cellContainerView addSubview:buttonLineBreakView];
    
    [_cellContainerArray addObject:cellContainerView];
    [_cellArrowArray addObject:@""];
}

/*
 * Layout UI of the menu buttons and sub-menu buttons
 *
 * Note : Too lazy to make comment for this one
 *        but it is obviously readable
 */

-(void)initSubMenuButtonCell:(NSArray*)array row:(NSInteger)row{
    
    UIView *cellContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, _cellHeight * row, _screenWidth, _cellHeight * array.count)];
    cellContainerView.backgroundColor = _menuBackgroundColor;
    [_scrollView addSubview:cellContainerView];
    
    /*
    UILabel *cellText = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, _screenWidth, _cellHeight)];
    cellText.text = [NSString stringWithFormat:@"%@", [array objectAtIndex:0]];
    cellText.textColor = _textColor;
    cellText.backgroundColor = _menuBackgroundColor;
    cellText.font = _font;
    [cellContainerView addSubview:cellText];
    cellText.frame = CGRectMake(10, 0, cellText.intrinsicContentSize.width, _cellHeight);
    */
    
    float logoAdjustment = 0.0;
    
    UIImage *logo_img = [UIImage imageNamed:[NSString stringWithFormat:@"%@", [_menu.logo objectAtIndex:row]]];
    
    if(logo_img == nil){
        
        logoAdjustment = 10;
    }else{
        
        logoAdjustment = _cellHeight + 30;
    }
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logo_img];
    logoImageView.frame = CGRectMake(20, 25 / 2, _cellHeight - 25, _cellHeight - 25);
    //logoImageView.backgroundColor = [UIColor orangeColor];
    logoImageView.contentMode = UIViewContentModeScaleAspectFill;
    logoImageView.clipsToBounds = YES;
    [cellContainerView addSubview:logoImageView];
    
    UILabel *cellText = [[UILabel alloc] initWithFrame:CGRectMake(logoAdjustment, 0, _screenWidth - logoAdjustment, _cellHeight)];
    cellText.text = [NSString stringWithFormat:@"%@", [array objectAtIndex:0]];
    cellText.textColor = _textColor;
    //cellText.backgroundColor = [UIColor cyanColor];
    cellText.backgroundColor = _menuBackgroundColor;
    cellText.font = _font;
    [cellContainerView addSubview:cellText];
    cellText.frame = CGRectMake(logoAdjustment, 0, cellText.intrinsicContentSize.width, _cellHeight);
    
    UIImage *arrowImage = [UIImage imageNamed:@"arrowRight"];
    float arrowSize = arrowImage.size.height / 8;
    
    UIImageView *dropDownArrowImageView = [[UIImageView alloc] initWithImage:arrowImage];
    dropDownArrowImageView.frame = CGRectMake(cellText.frame.origin.x + cellText.frame.size.width + 5, (_cellHeight / 2) - (arrowSize / 2), arrowSize, arrowSize);
    dropDownArrowImageView.contentMode = UIViewContentModeScaleToFill;
    dropDownArrowImageView.clipsToBounds = YES;
    [cellContainerView addSubview:dropDownArrowImageView];
    
    UIButton *cellButton = [UIButton buttonWithType:UIButtonTypeSystem];
    cellButton.frame = CGRectMake(0, 0, _screenWidth, _cellHeight);
    cellButton.tag = row;
    [cellContainerView addSubview:cellButton];
    
    [cellButton addTarget:self action:@selector(showSubMenuEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonLineBreakView = [[UIView alloc] initWithFrame:CGRectMake(10, cellButton.frame.size.height - 1, cellButton.frame.size.width - 20, 1)];
    buttonLineBreakView.backgroundColor = _linebreakColor;
    [cellContainerView addSubview:buttonLineBreakView];
    
    [_cellContainerArray addObject:cellContainerView];
    [_cellArrowArray addObject:dropDownArrowImageView];
    
    UIView *lineBreakView = [[UIView alloc] initWithFrame:CGRectMake(10, cellContainerView.frame.size.height - 1, cellContainerView.frame.size.width - 20, 1)];
    lineBreakView.backgroundColor = _linebreakColor;
    [cellContainerView addSubview:lineBreakView];
    
    UIView *subCellContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, _cellHeight, _screenWidth, _cellHeight * (array.count - 1))];
    subCellContainerView.backgroundColor = _subMenuBackgroundColor;
    [cellContainerView addSubview:subCellContainerView];
    
    for (int i = 1; i < array.count; i++) {
        UILabel *subCellText = [[UILabel alloc] initWithFrame:CGRectMake(80, (i - 1) * _cellHeight, _screenWidth - 20, _cellHeight)];
        subCellText.text = [NSString stringWithFormat:@"%@", [array objectAtIndex:i]];
        subCellText.textColor = _textColor;
        subCellText.font = _font;
        [subCellContainerView addSubview:subCellText];
        
        UIButton *subCellButton = [UIButton buttonWithType:UIButtonTypeSystem];
        subCellButton.frame = CGRectMake(0, (i - 1) * _cellHeight, _screenWidth, _cellHeight);
        //subCellButton.backgroundColor = [[UIColor cyanColor] colorWithAlphaComponent:0.8];
        subCellButton.tag = (row * 10) + i;
        [subCellContainerView addSubview:subCellButton];
        
        [subCellButton addTarget:self action:@selector(cellButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *subLineBreakView = [[UIView alloc] initWithFrame:CGRectMake(20, subCellButton.frame.size.height - 1, subCellButton.frame.size.width - 30, 1)];
        subLineBreakView.backgroundColor = _linebreakColor;
        [subCellButton addSubview:subLineBreakView];
    }
}

-(void)cellButtonEvent:(UIButton*)sender{
    //NSLog(@"sender from menu buttons : %ld",(long)sender.tag);
    
    /*
     * Delegates for all menu and sub-menu buttons
     *
     * Reads what we just click in the menu
     */
    
    /*
     * Toogle off or hide the menu controller
     */
    
    [self toogleMenuView];
    
    /*
     * Make weak self for block
     */
    
    __weak typeof(self) weakSelf = self;
    
    /*
     * Get row
     */
    
    NSInteger row = [weakSelf getNode:sender.tag dimension:0];
    
    /*
     * Get section
     */
    
    NSInteger section = [weakSelf getNode:sender.tag dimension:1];
    
    /*
     * Assign as object 1st so we can check if
     * its view controller or not
     */
    
    id object = _menu.viewControllers[row][section];

    /*
     * Check if object is view controller
     *
     * If not view controller then don't pass anything in delegate
     *
     * ELSE its a view controller then pass anything
     */
    
    if(![object isKindOfClass:[UIViewController class]]){
        NSLog(@"No view controller found");
    }else{
        UIViewController *vc = object;
        
        [self setMenuViewDisappear:^{
            
            [weakSelf.delegate didSelectMenu:weakSelf viewController:vc];
            
            /*
             * Remove the function so it wont repeat the process just incase
             * we click the backview of menu controller
             */
            
            _menuViewDisappear = nil;
        }];
    }
}

-(void)showSubMenuEvent:(UIButton*)sender{
    //NSLog(@"sender from sub-menu buttons : %ld",(long)sender.tag);
    
    /*
     * Get the sub-menu base on which row we just click
     * We need this for reference size or how many
     * there are in sub-menu so we can calculate on
     * how much pixels or height of buttons to move down
     */
    
    NSArray *subMenuArray = [_semiMatrixArray objectAtIndex:sender.tag];
    
    /*
     * Set always to zero when the event start so we
     * can calculate how many pixels we can move
     */
    
    float subMenuViewHeight = 0.0;
    
    /*
     * Get the bool value base on which row we click
     */
    
    BOOL toogle = [[_toogleArray objectAtIndex:sender.tag] boolValue];
    
    /*
     * Set always to zero when the event start so we
     * can calculate whether we rotate to 90 degrees vise versa
     */
    
    CGAffineTransform rotate90Degree = CGAffineTransformMakeRotation(0);;
    
    /*
     * Toogle condition for menu button with sub-menu button
     */
    
    if(!toogle){
        
        /*
         * If the toogle is false then after the condition
         * checking we set the toogle value to true
         */
        
        toogle = YES;
        
        /*
         * Set the height base on how many sub-menu buttons
         * but we are removing 1 count because we are not
         * including the menu button that we just click
         */
        
        subMenuViewHeight = ((subMenuArray.count - 1) * _cellHeight);
        
        /*
         * Set rotation value but for this we are rotating to
         * 90 degrees
         *
         * M_PI_2 is equivalent to Math-Pie / 2
         */
        
        rotate90Degree = CGAffineTransformMakeRotation(M_PI_2);
        
        //NSLog(@"toggle on = %i", toogle);
        
    }else if(toogle){
        
        /*
         * If the toogle is true then after the condition
         * checking we set the toogle value to false
         */
        
        toogle = NO;
        
        /*
         * This is just the same from the 1st/previous
         * condition but in negative value
         */
        
        subMenuViewHeight = -((subMenuArray.count - 1) * _cellHeight);
        
        /*
         * Set rotation value back to original
         */
        
        rotate90Degree = CGAffineTransformMakeRotation(M_PI_2 * 4);
        
        //NSLog(@"toggle off = %i", toogle);
    }
    
    //NSLog(@"subMenuViewHeight = %f",subMenuViewHeight);
    
    /*
     * We replace the toogle's bool value base on which row we click
     */
    
    [_toogleArray replaceObjectAtIndex:sender.tag withObject:[NSNumber numberWithBool:toogle]];
    
    for (int i = (int)sender.tag + 1 ; i < _cellContainerArray.count; i++) {
        
        /*
         * Get the menu button base on which row we click on
         */
        
        UIView *movingButtonCellView = [_cellContainerArray objectAtIndex:i];
        
        if(_collapseSpeed <= 0.0f){
            
            /*
             * Default speed
             */
            
            _collapseSpeed = 0.2;
        }
        
        /*
         * Animation of collapsing the sub-menu buttons
         */
        
        [UIView animateWithDuration:_collapseSpeed
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             /*
                              * Move the other buttons below of the menu button with sub-menu
                              */
                             
                             movingButtonCellView.frame = CGRectMake(movingButtonCellView.frame.origin.x, movingButtonCellView.frame.origin.y + subMenuViewHeight, movingButtonCellView.frame.size.width, movingButtonCellView.frame.size.height);
                             
                         }
                         completion:^(BOOL finished){
                             if(finished){
                                 
                                 /*
                                  * Calculating for the size of scrollview
                                  * then we change the content size of the scrollview
                                  * base on how many menu buttons and sub-menu buttons
                                  */
                                 
                                 /*
                                  * Note: In this case, the excess view should'nt affect the content
                                  * size of the scrollview because we are basing the total size of
                                  * all menu buttons and sub-menu buttons
                                  *
                                  * Also the header space is added here
                                  */
                                 
                                 UIView *lastButtonCellView = [_cellContainerArray objectAtIndex:_cellContainerArray.count - 1];
                                 UIView *excessView = [_cellContainerArray lastObject];
                                 
                                 _scrollView.contentSize = CGSizeMake(_screenWidth, ((lastButtonCellView.frame.origin.y + lastButtonCellView.frame.size.height) - (excessView.frame.size.height - _cellHeight)) + _headerSpace);
                             }
                         }];
    }
    
    /*
     * Get the arrow base on which row we click on
     */
    
    UIImageView *rotatingArrowImageView = [_cellArrowArray objectAtIndex:sender.tag];
    
    /*
     * Animation of rotating the arrow 90 degrees
     */
    
    [UIView animateWithDuration:_collapseSpeed
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         /*
                          * Rotate the arrow by 90 degrees
                          */
                         
                         rotatingArrowImageView.transform = rotate90Degree;
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                             /*
                              * Add something here after rotating the arrow
                              */
                             
                         }
                     }];
    
    [self.delegate didSelectSubMenu:self];
}

-(NSInteger)getNode:(NSInteger)number dimension:(NSInteger)dimension{
    
    NSString *intToStr = [NSString stringWithFormat:@"%ld",(long)number];
    
    NSMutableArray *mutalbleArray = [[NSMutableArray alloc] initWithCapacity:intToStr.length];
    for (int i = 0; i < intToStr.length; i++) {
        intToStr  = [NSString stringWithFormat:@"%c", [intToStr characterAtIndex:i]];
        [mutalbleArray addObject:intToStr];
    }
    
    if(mutalbleArray.count == 1){
        [mutalbleArray addObject:@"0"];
    }
    
    intToStr = [mutalbleArray objectAtIndex:dimension];

    return [intToStr integerValue];
}

/*
 * Load settings for menu controller
 */

-(void)loadSettings{
    _popToFront = NO;
    
    _cellHeight = [[NSUserDefaults standardUserDefaults] floatForKey:@"setting_height"];
    
    NSData *fontData = [[NSUserDefaults standardUserDefaults] objectForKey:@"setting_font"];
    _font = [NSKeyedUnarchiver unarchiveObjectWithData:fontData];
    
    NSData *textColorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"setting_textColor"];
    _textColor = [NSKeyedUnarchiver unarchiveObjectWithData:textColorData];
    
    NSData *menuBackgroundColorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"setting_menuBackgroundColor"];
    _menuBackgroundColor = [NSKeyedUnarchiver unarchiveObjectWithData:menuBackgroundColorData];
    
    NSData *subMenuBackgroundColorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"setting_subMenuBackgroundColor"];
    _subMenuBackgroundColor = [NSKeyedUnarchiver unarchiveObjectWithData:subMenuBackgroundColorData];
    
    NSData *linebreakColorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"setting_linebreakColor"];
    _linebreakColor = [NSKeyedUnarchiver unarchiveObjectWithData:linebreakColorData];
}
@end
