//
//  TimeConverter.h
//  UOB Summit
//
//  Created by LF-Mac-Air on 29/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeConverter : NSObject

/*
 * Convert the server time base to standard time
 * Ex : 2016-07-26T23:01:36+08:00 => 11:01 PM
 */

-(NSString*)serverTimeFormatToDayTime:(NSString*)time;

/*
 * Convert the server date base to standard date
 * Ex : 2016-07-26T23:01:36+08:00 => Jul 26 2016
 */

-(NSString*)serverDateFormatToDate:(NSString*)date;

@end
