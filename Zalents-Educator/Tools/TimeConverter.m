//
//  TimeConverter.m
//  UOB Summit
//
//  Created by LF-Mac-Air on 29/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "TimeConverter.h"

@interface TimeConverter()

@property (nonatomic, strong) NSCharacterSet *cutString;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation TimeConverter

-(instancetype)init{
    self = [super init];
    
    if(self){
        
        /*
         * Init set of characters to be remove
         */
        
        _cutString = [NSCharacterSet characterSetWithCharactersInString:@"T+"];
        
        /*
         * Init date formatter
         */
        
        _dateFormatter = [[NSDateFormatter alloc] init];
    }
    
    return self;
}

-(NSString*)serverTimeFormatToDayTime:(NSString*)time{
    
    /*
     * Check if time is null
     * If null : return string with "Time has not been set"
     * If not null : proceed on converting server time full date
     * to standard time
     */
    
    if (time == (id)[NSNull null]) {
        time = @"Time has not been set";
    }else{
        
        /*
         * Replace those characters just set to be removed with space
         */
        
        time = [[time componentsSeparatedByCharactersInSet: _cutString] componentsJoinedByString: @" "];
        
        /*
         * We seperate them then convert it to array
         */
        
        NSArray *timeCutArray = [time componentsSeparatedByString:@" "];
        
        /*
         * Set new time value base on index 1 on array
         * Check inside to see all value
         */
        
        time = [timeCutArray objectAtIndex:1];
        
        //_dateFormatter = [[NSDateFormatter alloc] init];
        
        /*
         * Set date format base on the time string value
         */
        
        [_dateFormatter setDateFormat:@"HH:mm:ss"];
        
        /*
         * Convert the time string to raw object as NSDate
         */
        
        NSDate* rawTime = [_dateFormatter dateFromString:time];
        
        /*
         * Set date format to be converted from military time
         * to standard time with AM/PM
         */
        
        [_dateFormatter setDateFormat:@"hh:mm a"];
        
        /*
         * Convert the raw object of NSDate to string
         */
        
        time = [_dateFormatter stringFromDate:rawTime];
    }
    
    return time;
}

-(NSString*)serverDateFormatToDate:(NSString*)date{
    
    /*
     * This is just the same process to the "serverTimeFormatToDayTime" method
     * the only difference is that we choose the 1st index of array or
     * index object at 0
     *
     * Date formatter is also noticeable, we just change the
     * order of the date and convert months number to chars
     */
    
    if (date == nil) {
        date = @"Date has not been set";
    }else{
        date = [[date componentsSeparatedByCharactersInSet: _cutString] componentsJoinedByString: @" "];
        NSArray *dateCutArray = [date componentsSeparatedByString:@" "];
        date = [dateCutArray objectAtIndex:0];
        
        [_dateFormatter setDateFormat:@"yyyy-mm-dd"];
        NSDate* rawDate = [_dateFormatter dateFromString:date];
        
        [_dateFormatter setDateFormat:@"MMM dd yyyy"];
        date = [_dateFormatter stringFromDate:rawDate];
    }
    
    return  date;
}
@end
