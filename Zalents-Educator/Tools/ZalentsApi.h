//
//  ZalentsApi.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 29/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^result)(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse);

@interface ZalentsApi : NSObject

-(void)login:(NSString*)url username:(NSString*)username password:(NSString*)password isComplete:(result)check;

-(void)registration:(NSString*)url
         accessCode:(NSString*)accessCode
          firstName:(NSString*)firstName
           lastName:(NSString*)lastName
              email:(NSString*)email
           password:(NSString*)password
passwordConfirmation:(NSString*)passwordConfirmation
             gender:(NSString*)gender
       mobileNumber:(NSString*)mobileNumber
        countryCode:(NSString*)countryCode
           birthday:(NSString*)birthday
              terms:(NSString*)terms
         isComplete:(result)check;

@end
