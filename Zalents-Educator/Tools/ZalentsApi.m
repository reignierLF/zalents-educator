//
//  ZalentsApi.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 29/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ZalentsApi.h"

@implementation ZalentsApi

-(instancetype)init{
    
    self = [super init];
    
    if(self){
        
    }
    
    return self;
}

-(void)login:(NSString*)url username:(NSString*)username password:(NSString*)password isComplete:(result)check{
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"3f2f69aa-80cf-adaf-2bc4-9113e280072a" };
    NSDictionary *parameters = @{ @"user": @{ @"email": username, @"password": password } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/users/sessions.json",url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                        
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)registration:(NSString*)url
         accessCode:(NSString*)accessCode
          firstName:(NSString*)firstName
           lastName:(NSString*)lastName
              email:(NSString*)email
           password:(NSString*)password
passwordConfirmation:(NSString*)passwordConfirmation
             gender:(NSString*)gender
       mobileNumber:(NSString*)mobileNumber
        countryCode:(NSString*)countryCode
           birthday:(NSString*)birthday
              terms:(NSString*)terms
         isComplete:(result)check{
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"f5da9c1c-eea8-8e52-944f-f4e006d89d2e" };
    
    NSDictionary *parameters = @{ @"access_code": accessCode,
                                  @"user": @{ @"first_name": firstName,
                                              @"last_name": lastName,
                                              @"email": email,
                                              @"password": password,
                                              @"password_confirmation": passwordConfirmation,
                                              @"gender": gender,
                                              @"mobile_number":mobileNumber,
                                              @"country_code":countryCode,
                                              @"birthdate": birthday,
                                              @"terms": terms } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/users/registrations.json",url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getEvents:(NSString*)url isComplete:(result)check{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"c38cabfa-4724-3d5f-ddbf-b36a3f443d1d" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/events.json",url]] cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        check(YES, data, error, response, httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)payment:(NSString*)url email:(NSString*)email token:(NSString*)token eventBatchId:(NSString*)eventBatchId isComplete:(result)check{
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"108c8133-92fa-85ea-8bdd-2dd6263af5f4" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user_events/new.json?user_email=%@&user_token=%@&event_batch_id=%@",url,email,token,eventBatchId]] cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)book:(NSString*)url email:(NSString*)email {

}
@end
