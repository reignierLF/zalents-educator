//
//  BookingViewController.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 16/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Stripe/Stripe.h>

@interface BookingViewController : UIViewController <STPAddCardViewControllerDelegate>

@property (nonatomic, strong) NSString *eventId;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *eventTitle;

@end
