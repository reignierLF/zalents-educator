//
//  BookingViewController.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 16/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "BookingViewController.h"
#import "cUIScrollView.h"
#import "BatchesView.h"
#import "ListDatesView.h"

#import "ImageLoader.h"

#import "JsonParser.h"
#import "Events.h"

@interface BookingViewController () <UIScrollViewDelegate, BatchesViewDelegate, ListDatesViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) JsonParser *json;

@property (nonatomic, strong) cUIScrollView *bookingPagerScrollView;
//@property (nonatomic, strong) cUIScrollView *eventBatchesscrollView;

@property (nonatomic, strong) UIView *datesChoicesBackgroundView;
@property (nonatomic, strong) ListDatesView *listDatesView;
@property (nonatomic, strong) BatchesView *bView;

@property (nonatomic, strong) NSMutableArray *dateArray;
@property (nonatomic, strong) NSMutableArray *startTimeArray;
@property (nonatomic, strong) NSMutableArray *endTimeArray;
@property (nonatomic, strong) NSMutableArray *availableSlotsArray;
@property (nonatomic, strong) NSMutableArray *maxSlotsArray;
@property (nonatomic, strong) NSMutableArray *venueArray;

@end

@implementation BookingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initEventDetailScrollView];
    [self fetchDataFromBackend];
    //[self initEventDetails];
    //[self initPayment];
    [self initDatesChoicesLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.tintColor = nil;
    self.navigationController.navigationBar.translucent = NO;
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.translucent = YES;
    self.navigationItem.hidesBackButton = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    self.navigationItem.titleView = nil;
    self.title = @"Events";
    
    _json = [[JsonParser alloc] init];
    
    _dateArray = [[NSMutableArray alloc] init];
    _startTimeArray = [[NSMutableArray alloc] init];
    _endTimeArray = [[NSMutableArray alloc] init];
    _availableSlotsArray = [[NSMutableArray alloc] init];
    _maxSlotsArray = [[NSMutableArray alloc] init];
    _venueArray = [[NSMutableArray alloc] init];
}

-(void)initEventDetailScrollView{

    _bookingPagerScrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    _bookingPagerScrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    _bookingPagerScrollView.delegate = self;
    _bookingPagerScrollView.contentSize = CGSizeMake(_bookingPagerScrollView.frame.size.width, _screenHeight);
    _bookingPagerScrollView.showsHorizontalScrollIndicator = YES;
    [self.view addSubview:_bookingPagerScrollView];
    
//    _eventBatchesscrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
//    _eventBatchesscrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
//    _eventBatchesscrollView.contentSize = CGSizeMake(_eventBatchesscrollView.frame.size.width, _eventBatchesscrollView.frame.size.height);
//    _eventBatchesscrollView.delaysContentTouches = NO;
//    [_bookingPagerScrollView addSubview:_eventBatchesscrollView];
}

-(void)initEventDetails{
    
    UIView *eventView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _bookingPagerScrollView.frame.size.width, _screenHeight / 3)];
    //featureView.backgroundColor = [UIColor orangeColor];
    [_bookingPagerScrollView addSubview:eventView];
    
    UIImageView *eventImageView = [[UIImageView alloc] init];
    eventImageView.frame = CGRectMake(0, 0, eventView.frame.size.width, eventView.frame.size.height);
    //_eventImageView.backgroundColor = [UIColor orangeColor];
    eventImageView.contentMode = UIViewContentModeScaleAspectFill;
    eventImageView.clipsToBounds = YES;
    [eventView addSubview:eventImageView];
    
    ImageLoader *il = [[ImageLoader alloc] init];
    [il parseImage:eventImageView url:_imageUrl];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, eventImageView.frame.size.height, eventImageView.frame.size.width, 60)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.layer.masksToBounds = NO;
    headerView.layer.shadowOffset = CGSizeMake(-2, 2);
    headerView.layer.shadowRadius = 5;
    headerView.layer.shadowOpacity = 0.2;
    [_bookingPagerScrollView addSubview:headerView];
    
    UILabel *titleHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, headerView.frame.size.width - 60, headerView.frame.size.height)];
    titleHeaderLabel.text = _eventTitle;
    titleHeaderLabel.textAlignment = NSTextAlignmentCenter;
    titleHeaderLabel.numberOfLines = 0;
    titleHeaderLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleHeaderLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
    titleHeaderLabel.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    [headerView addSubview:titleHeaderLabel];
    
    //[self fetchDataFromBackend];

    _bView = [[BatchesView alloc] initWithRelativeView:headerView];
    //bView.backgroundColor = [UIColor orangeColor];
    _bView.delegate = self;
    _bView.iconSize = 20;
    [_bookingPagerScrollView addSubview:_bView];
    
    [self updateBookSchedule:0];
    
//    NSString *date = [_dateArray objectAtIndex:0];
//    NSString *time = [NSString stringWithFormat:@"%@ - %@",[_startTimeArray objectAtIndex:0],[_endTimeArray objectAtIndex:0]];
//    NSString *slot = [NSString stringWithFormat:@"%@ / %@",[_availableSlotsArray objectAtIndex:0],[_maxSlotsArray objectAtIndex:0]];
//    NSString *venue = [_venueArray objectAtIndex:0];
//    
//    NSArray *detailArray = @[date, time, slot, venue];
//    
//    for (int i = 0; i < bView.labelArray.count; i++) {
//        UILabel *newLabel = [bView.labelArray objectAtIndex:i];
//        newLabel.text = [detailArray objectAtIndex:i];
//    }
    
    _bookingPagerScrollView.contentSize = CGSizeMake(_bookingPagerScrollView.frame.size.width, _bView.frame.origin.y + _bView.frame.size.height + self.navigationController.navigationBar.frame.size.height);
}

-(void)updateBookSchedule:(NSInteger)index{
    NSString *date = [NSString stringWithFormat:@"%@",[_dateArray objectAtIndex:index]];
    NSString *time = [NSString stringWithFormat:@"%@ - %@",[_startTimeArray objectAtIndex:index],[_endTimeArray objectAtIndex:index]];
    NSString *slot = [NSString stringWithFormat:@"%@ / %@",[_availableSlotsArray objectAtIndex:index],[_maxSlotsArray objectAtIndex:index]];
    NSString *venue = [_venueArray objectAtIndex:index];
    
    NSArray *detailArray = @[date, time, slot, venue];
    
    for (int i = 0; i < _bView.labelArray.count; i++) {
        UILabel *newLabel = [_bView.labelArray objectAtIndex:i];
        newLabel.text = [detailArray objectAtIndex:i];
    }
}

-(void)fetchDataFromBackend{
    
    [_json parseJsonURL:@"http://testedu.everythingdemo.com/api/v1/events.json" parsing:^(BOOL isComplete){
        
        if(isComplete){
            
            for (int i = 0; i < _json.jsonArray.count; i++) {
                
                Events *events = [[Events alloc] initWithDictionary:[_json.jsonArray objectAtIndex:i]];
                
                if([events.eventsId integerValue] == [_eventId integerValue]){
                    
                    NSInteger size = events.eventBatches.eventBatchesEventId.count;
                    
                    NSLog(@"size = %ld",(long)size);
                    
                    for (int j = 0; j < size; j++) {
                        
                        [_dateArray addObject:[events.eventBatches.eventBatchesStartedAt objectAtIndex:j]];
                        [_startTimeArray addObject:[events.eventBatches.eventBatchesStartTime objectAtIndex:j]];
                        [_endTimeArray addObject:[events.eventBatches.eventBatchesEndTime objectAtIndex:j]];
                        [_availableSlotsArray addObject:[events.eventBatches.eventBatchesSlot objectAtIndex:j]];
                        [_maxSlotsArray addObject:[events.eventBatches.eventBatchesSlot objectAtIndex:j]];
                        [_venueArray addObject:[events.eventBatches.eventBatchesVenue1 objectAtIndex:j]];
                    }
                    
                    [self initListDatesLayout];
                    [self initEventDetails];
                }
            }
            
            NSLog(@"complete");
        }else{
            
            NSLog(@"failed");
        }
    }];
}

-(void)initDatesChoicesLayout{
    
    UIWindow *appWindow = [UIApplication sharedApplication].keyWindow;
    
    _datesChoicesBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, appWindow.frame.size.width, appWindow.frame.size.height)];
    _datesChoicesBackgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    _datesChoicesBackgroundView.alpha = 0;
    [appWindow addSubview:_datesChoicesBackgroundView];
    
    UIButton *hideDatesChoicesBackgroundButton = [UIButton buttonWithType:UIButtonTypeSystem];
    hideDatesChoicesBackgroundButton.frame = _datesChoicesBackgroundView.bounds;
    [_datesChoicesBackgroundView addSubview:hideDatesChoicesBackgroundButton];
    
    [hideDatesChoicesBackgroundButton addTarget:self action:@selector(hideDatesChoicesBackground) forControlEvents:UIControlEventTouchUpInside];
}

-(void)initListDatesLayout{
    
    float width = _datesChoicesBackgroundView.frame.size.width - (_datesChoicesBackgroundView.frame.size.width / 4);
    float height = _datesChoicesBackgroundView.frame.size.height - (_datesChoicesBackgroundView.frame.size.height / 4);
    
    _listDatesView = [[ListDatesView alloc] initWithFrame:CGRectMake((_datesChoicesBackgroundView.frame.size.width / 2) - (width / 2), (_datesChoicesBackgroundView.frame.size.height / 2) - (height / 2), width, height)];
    _listDatesView.backgroundColor = [UIColor whiteColor];
    _listDatesView.delegate = self;
    _listDatesView.dateArray = _dateArray;
    _listDatesView.startTimeArray = _startTimeArray;
    _listDatesView.endTimeArray = _endTimeArray;
    [_datesChoicesBackgroundView addSubview:_listDatesView];
}

/*
-(void)initPayment{
    UIView *featureView = [[UIView alloc] initWithFrame:CGRectMake(_bookingPagerScrollView.frame.size.width, 0, _bookingPagerScrollView.frame.size.width, _screenHeight / 3)];
    featureView.backgroundColor = [UIColor orangeColor];
    [_bookingPagerScrollView addSubview:featureView];
    
    UIImageView *eventImageView = [[UIImageView alloc] init];
    eventImageView.frame = CGRectMake(0, 0, featureView.frame.size.width, featureView.frame.size.height);
    //_eventImageView.backgroundColor = [UIColor orangeColor];
    eventImageView.contentMode = UIViewContentModeScaleAspectFill;
    eventImageView.clipsToBounds = YES;
    [featureView addSubview:eventImageView];
    
    UIView *detailView = [[UIView alloc] initWithFrame:CGRectMake(_bookingPagerScrollView.frame.size.width, eventImageView.frame.size.height, eventImageView.frame.size.width, _screenHeight / 5)];
    detailView.backgroundColor = [UIColor whiteColor];
    [_bookingPagerScrollView addSubview:detailView];
    
    UILabel *confirmationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, detailView.frame.size.width, 40)];
    //confirmationLabel.backgroundColor = [UIColor cyanColor];
    confirmationLabel.text = @"Confirm this booking?";
    confirmationLabel.font = [UIFont boldSystemFontOfSize:20];
    confirmationLabel.textAlignment = NSTextAlignmentCenter;
    [detailView addSubview:confirmationLabel];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, confirmationLabel.frame.origin.y + confirmationLabel.frame.size.height, detailView.frame.size.width - 40, 15)];
    //titleLabel.backgroundColor = [UIColor cyanColor];
    titleLabel.text = @"title";
    titleLabel.font = [UIFont boldSystemFontOfSize:15];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [detailView addSubview:titleLabel];
    
    UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, titleLabel.frame.origin.y + titleLabel.frame.size.height, detailView.frame.size.width - 80, detailView.frame.size.height - (titleLabel.frame.origin.y + titleLabel.frame.size.height))];
    //locationLabel.backgroundColor = [UIColor cyanColor];
    locationLabel.text = @"location";
    locationLabel.font = [UIFont systemFontOfSize:12];
    locationLabel.textAlignment = NSTextAlignmentCenter;
    locationLabel.numberOfLines = 0;
    locationLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [detailView addSubview:locationLabel];
    
    UIView *creditView = [[UIView alloc] initWithFrame:CGRectMake(_bookingPagerScrollView.frame.size.width,detailView.frame.origin.y + detailView.frame.size.height, eventImageView.frame.size.width, _screenHeight / 5)];
    //creditView.backgroundColor = [UIColor greenColor];
    [_bookingPagerScrollView addSubview:creditView];
    
    UILabel *balanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, (creditView.frame.size.width / 2) - 20, 20)];
    //balanceLabel.backgroundColor = [UIColor cyanColor];
    balanceLabel.text = @"Balance";
    balanceLabel.font = [UIFont systemFontOfSize:15];
    balanceLabel.textAlignment = NSTextAlignmentLeft;
    [creditView addSubview:balanceLabel];
    
    UILabel *currentBalanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(creditView.frame.size.width - ((creditView.frame.size.width / 2) + 20), 10, creditView.frame.size.width / 2, 20)];
    //currentBalanceLabel.backgroundColor = [UIColor cyanColor];
    currentBalanceLabel.text = @"0 credits";
    currentBalanceLabel.font = [UIFont systemFontOfSize:15];
    currentBalanceLabel.textAlignment = NSTextAlignmentRight;
    [creditView addSubview:currentBalanceLabel];
    
    UILabel *costLabel = [[UILabel alloc] initWithFrame:CGRectMake(balanceLabel.frame.origin.x, balanceLabel.frame.origin.y + balanceLabel.frame.size.height, balanceLabel.frame.size.width, balanceLabel.frame.size.height)];
    costLabel.backgroundColor = [UIColor whiteColor];
    costLabel.text = @"Cost:";
    costLabel.font = [UIFont systemFontOfSize:15];
    costLabel.textAlignment = NSTextAlignmentLeft;
    [creditView addSubview:costLabel];
    
    UILabel *currentCostLabel = [[UILabel alloc] initWithFrame:CGRectMake(creditView.frame.size.width - ((creditView.frame.size.width / 2) + 20), currentBalanceLabel.frame.origin.y + currentBalanceLabel.frame.size.height, creditView.frame.size.width / 2, 20)];
    //currentBalanceLabel.backgroundColor = [UIColor cyanColor];
    currentCostLabel.text = @"0 credits";
    currentCostLabel.font = [UIFont systemFontOfSize:15];
    currentCostLabel.textAlignment = NSTextAlignmentRight;
    [creditView addSubview:currentCostLabel];
    
    UILabel *discountLabel = [[UILabel alloc] initWithFrame:CGRectMake(costLabel.frame.origin.x, costLabel.frame.origin.y + costLabel.frame.size.height, costLabel.frame.size.width, costLabel.frame.size.height)];
    //discountLabel.backgroundColor = [UIColor cyanColor];
    discountLabel.text = @"Discount:";
    discountLabel.font = [UIFont systemFontOfSize:15];
    discountLabel.textAlignment = NSTextAlignmentLeft;
    [creditView addSubview:discountLabel];
    
    UILabel *currentDiscountLabel = [[UILabel alloc] initWithFrame:CGRectMake(creditView.frame.size.width - ((creditView.frame.size.width / 2) + 20), currentCostLabel.frame.origin.y + currentCostLabel.frame.size.height, creditView.frame.size.width / 2, 20)];
    //currentBalanceLabel.backgroundColor = [UIColor cyanColor];
    currentDiscountLabel.text = @"0 credits";
    currentDiscountLabel.font = [UIFont systemFontOfSize:15];
    currentDiscountLabel.textAlignment = NSTextAlignmentRight;
    [creditView addSubview:currentDiscountLabel];
    
    UILabel *totalLabel = [[UILabel alloc] initWithFrame:CGRectMake(discountLabel.frame.origin.x, discountLabel.frame.origin.y + discountLabel.frame.size.height, discountLabel.frame.size.width, discountLabel.frame.size.height)];
    //totalLabel.backgroundColor = [UIColor cyanColor];
    totalLabel.text = @"Total:";
    totalLabel.font = [UIFont systemFontOfSize:15];
    totalLabel.textAlignment = NSTextAlignmentLeft;
    [creditView addSubview:totalLabel];
    
    UILabel *currentTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(creditView.frame.size.width - ((creditView.frame.size.width / 2) + 20), currentDiscountLabel.frame.origin.y + currentDiscountLabel.frame.size.height, creditView.frame.size.width / 2, 20)];
    //currentBalanceLabel.backgroundColor = [UIColor cyanColor];
    currentTotalLabel.text = @"0 credits";
    currentTotalLabel.font = [UIFont systemFontOfSize:15];
    currentTotalLabel.textAlignment = NSTextAlignmentRight;
    [creditView addSubview:currentTotalLabel];
    
    UIView *confirmationView = [[UIView alloc] initWithFrame:CGRectMake(_bookingPagerScrollView.frame.size.width,creditView.frame.origin.y + creditView.frame.size.height, eventImageView.frame.size.width, _screenHeight - (creditView.frame.origin.y + creditView.frame.size.height))];
    //confirmationView.backgroundColor = [UIColor blueColor];
    [_bookingPagerScrollView addSubview:confirmationView];
}
*/

-(void)didShowDatesChoices{
    
    [UIView transitionWithView:_datesChoicesBackgroundView
                      duration:0.3f
                       options:UIViewAnimationOptionCurveEaseInOut
                    animations:^{
                        _datesChoicesBackgroundView.alpha = 1;
                    }completion:nil];
    
    [_listDatesView animationShow:^(BOOL isComplete){
        
    }];
}

-(void)hideDatesChoicesBackground{
    
    [_listDatesView animationHide:^(BOOL isComplete){
        
        [UIView transitionWithView:_datesChoicesBackgroundView
                          duration:0.3f
                           options:UIViewAnimationOptionCurveEaseInOut
                        animations:^{
                            _datesChoicesBackgroundView.alpha = 0;
                        }completion:nil];
    }];
}

-(void)didSelectDate:(UIButton*)sender{
    
    [self hideDatesChoicesBackground];
    
    [self updateBookSchedule:sender.tag];
    
    NSLog(@"sender tag = %ld", (long)sender.tag);
}

-(void)paymentEvent:(BatchesView *)batchesView{
    
    STPAddCardViewController *addCardViewController = [[STPAddCardViewController alloc] init];
    addCardViewController.delegate = self;
    // STPAddCardViewController must be shown inside a UINavigationController.
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addCardViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
    
//    STPCardParams *cardParams = [[STPCardParams alloc] init];
//    cardParams.number = @"4242424242424242";
//    cardParams.expMonth = 10;
//    cardParams.expYear = 2018;
//    cardParams.cvc = @"123";
//    [[STPAPIClient sharedClient] createTokenWithCard:cardParams completion:^(STPToken *token, NSError *error) {
//        if (error) {
//            // show the error, maybe by presenting an alert to the user
//        } else {
//            
//            NSLog(@"TOKEN to be submit to backend = %@", token);
//            
//            /*
//            [self submitTokenToBackend:token completion:^(NSError *error) {
//                if (error) {
//                    // show the error
//                } else {
//                    [self showReceiptPage];
//                }
//            }];
//             */
//        }
//    }];
}


- (void)addCardViewControllerDidCancel:(STPAddCardViewController *)addCardViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addCardViewController:(STPAddCardViewController *)addCardViewController
               didCreateToken:(STPToken *)token
                   completion:(STPErrorBlock)completion {
    [self dismissViewControllerAnimated:YES completion:^{
         NSLog(@"TOKEN = %@", token);
    }];
   
//    [self submitTokenToBackend:token completion:^(NSError *error) {
//        if (error) {
//            completion(error);
//        } else {
//            [self dismissViewControllerAnimated:YES completion:^{
//                [self showReceiptPage];
//            }];
//        }
//    }];
}

@end
