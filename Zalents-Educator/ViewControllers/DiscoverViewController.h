//
//  DiscoverViewController.h
//  Zalents-Educator
//
//  Created by Livefitter on 28/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoverViewController : UIViewController<UIScrollViewDelegate>

@end
