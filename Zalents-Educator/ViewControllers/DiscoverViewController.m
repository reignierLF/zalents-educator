//
//  DiscoverViewController.m
//  Zalents-Educator
//
//  Created by Livefitter on 28/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "DiscoverViewController.h"
#import "JsonParser.h"
#import "MenuController.h"
#import "Menu.h"
#import "cUIScrollView.h"
#import "FeatureView.h"
#import "Events.h"

@interface DiscoverViewController ()<MenuControllerDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic, strong) MenuController *mc;


@property (nonatomic, strong) UIScrollView *pagerScrollView;
@property (nonatomic, strong) cUIScrollView *scrollView;
@property (nonatomic, strong) UIView *spaceView;

@property (nonatomic) NSInteger fakeAllEventsCounter;

@property (nonatomic) NSInteger featureCounter;
@property (nonatomic) NSInteger eventsCounter;

@property (nonatomic, strong) NSMutableArray *imageMediumUrlArray;
@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) NSMutableArray *dateArray;
@property (nonatomic, strong) NSMutableArray *addressArray;

@property (nonatomic, strong) UIPageControl *pageControl;

@end

@implementation DiscoverViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self initialize];
    [self initHomeScrollView];
    [self initFeatureLayout];
    [self initMenuController];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.translucent = NO;
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height - (self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height);
    
    
    JsonParser *json = [[JsonParser alloc] init];
    
    _imageMediumUrlArray = [[NSMutableArray alloc] init];
    _titleArray = [[NSMutableArray alloc] init];
    _dateArray = [[NSMutableArray alloc] init];
    _addressArray = [[NSMutableArray alloc] init];
    
    [json parseJsonURL:@"http://testedu.everythingdemo.com/api/v1/events.json" parsing:^(BOOL isComplete){
        
        if(isComplete){
            
            for (int i = 0; i < json.jsonArray.count; i++) {
                
                Events *events = [[Events alloc] initWithDictionary:[json.jsonArray objectAtIndex:i]];
                
                [_imageMediumUrlArray addObject:events.eventsImageMediumUrl];
                [_titleArray addObject:events.eventsTitle];
                [_dateArray addObject:@"01/20/2016 test"];
                [_addressArray addObject:@"7 Jalan Kilang"];
            }
            
            _featureCounter = _titleArray.count / 2;
            _eventsCounter = _titleArray.count;
            
            _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 80 + (((_screenHeight / 3) + 60) * (_eventsCounter + 1)));
            
            _pageControl.numberOfPages = _featureCounter;
            
            _pagerScrollView.contentSize = CGSizeMake(_pagerScrollView.frame.size.width * _featureCounter, _pagerScrollView.frame.size.height);
            
            for (int i = 0; i < _featureCounter; i++) {
                
                FeatureView *featureView = [[FeatureView alloc] initWithFrame:CGRectMake(_pagerScrollView.frame.size.width * i, 0, _pagerScrollView.frame.size.width, _pagerScrollView.frame.size.height)];
                //featureView.backgroundColor = [UIColor orangeColor];
                featureView.delegate = self;
                featureView.imageUrl = [NSString stringWithFormat:@"%@",[_imageMediumUrlArray objectAtIndex:i]];
                featureView.tag = i;
                [_pagerScrollView addSubview:featureView];
            }
            
            [self initEventsLayout];
            
            NSLog(@"page counter = %ld", (long)_pageControl.numberOfPages);
            NSLog(@"complete");
        }else{
            
            NSLog(@"failed");
        }
    }];
    
    
    
    
}
-(void)initMenuController{
    
    _mc = [[MenuController alloc] initWithFrame:CGRectMake(-(_screenWidth - (_screenWidth / 4)), 0, _screenWidth - (_screenWidth / 4), _screenHeight)];
    _mc.delegate = self;
    
    //_mc.popToFront = YES;
    //_mc.headerSpace = 130;
    //_mc.menu.settings.textColor = [UIColor colorWithRed:5/255.f green:11/255.f blue:103/255.f alpha:1.0];
    //_mc.menu.settings.menuBackgroundColor = [UIColor greenColor];
    [self.view addSubview:_mc];
    //UIView *testvie
    self.navigationItem.titleView = nil;
    
    NSLog(@"list from matrix = %@",_mc.matrixToList);
}
-(void)didSelectMenu:(MenuController *)menuController viewController:(UIViewController *)viewController{
    
    NSLog(@"viewController = %@",viewController);
    
    if([self.title isEqualToString:viewController.title]){
        NSLog(@"I'am clicking the view controller that is already presented");
    }else{
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(void)didSelectSubMenu:(MenuController *)menuController{
    NSLog(@"Did select sub menu");
}
-(void)initHomeScrollView{
    
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    _scrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 80 + (((_screenHeight / 3) + 60) * (_fakeAllEventsCounter + 1)));
    _scrollView.delaysContentTouches = NO;
    [self.view addSubview:_scrollView];
}

-(void)initFeatureLayout{
    
    _pagerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, (_screenHeight / 3) + 60)];
    //pagerScrollView.backgroundColor = [UIColor greenColor];
    _pagerScrollView.delegate = self;
    _pagerScrollView.bounces = NO;
    _pagerScrollView.pagingEnabled = YES;
    //_pagerScrollView.contentSize = CGSizeMake(_pagerScrollView.frame.size.width * _featureCounter, _pagerScrollView.frame.size.height);
    _pagerScrollView.showsHorizontalScrollIndicator = NO;
    //[_scrollView addSubview:_pagerScrollView];
    
    /*
     for (int i = 0; i < _featureCounter; i++) {
     
     FeatureView *featureView = [[FeatureView alloc] initWithFrame:CGRectMake(_pagerScrollView.frame.size.width * i, 0, _pagerScrollView.frame.size.width, _pagerScrollView.frame.size.height)];
     //featureView.backgroundColor = [UIColor orangeColor];
     featureView.delegate = self;
     featureView.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[_imageMediumUrlArray objectAtIndex:i]]];
     featureView.tag = i;
     [_pagerScrollView addSubview:featureView];
     }
     */
    
    UIImage *featureBannerimage = [UIImage imageNamed:@"featureBanner150px"];
    
    UIImageView *featureBannerImageView = [[UIImageView alloc] initWithImage:featureBannerimage];
    featureBannerImageView.frame = CGRectMake(0, 0, featureBannerimage.size.width, featureBannerimage.size.height);
    //featureImageView.backgroundColor = [UIColor orangeColor];
    featureBannerImageView.contentMode = UIViewContentModeScaleAspectFit;
    featureBannerImageView.clipsToBounds = YES;
    //[_scrollView addSubview:featureBannerImageView];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, _pagerScrollView.frame.size.height - 30, _screenWidth, 30)];
    _pageControl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    _pageControl.numberOfPages = _featureCounter;
    _pageControl.autoresizingMask = UIViewAutoresizingNone;
    //[_scrollView addSubview:_pageControl];
    
    _spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, _pagerScrollView.frame.origin.y + _pagerScrollView.frame.size.height, _screenWidth, 50)];
    _spaceView.backgroundColor = [UIColor whiteColor];
    //[_scrollView addSubview:_spaceView];
}

-(void)initEventsLayout{
    
//    for (int i = 0; i < _eventsCounter; i++) {
//        
//        ListEventView *eventView = [[ListEventView alloc] initWithFrame:CGRectMake(0, (((_screenHeight / 3) + 60) * i) + _spaceView.frame.size.height , _pagerScrollView.frame.size.width, (_screenHeight / 3) + 200)];
//        //eventView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.3];
//        eventView.delegate = self;
//        eventView.imageUrl = [NSString stringWithFormat:@"%@",[_imageMediumUrlArray objectAtIndex:i]];
//        eventView.tag = i;
//        eventView.title = [_titleArray objectAtIndex:i];
//        eventView.date = [_dateArray objectAtIndex:i];
//        eventView.address = [_addressArray objectAtIndex:i];
//        [_scrollView addSubview:eventView];
//    }
}

@end
