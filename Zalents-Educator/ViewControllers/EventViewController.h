//
//  EventViewController.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 15/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface EventViewController : UIViewController

@property (nonatomic, strong) NSString *eventId;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *eventTitle;
@property (nonatomic, strong) NSString *eventDescription;

@end
