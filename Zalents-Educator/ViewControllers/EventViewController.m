//
//  EventViewController.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 15/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "EventViewController.h"
#import "cUIScrollView.h"

#import "BookingViewController.h"

#import "ImageLoader.h"

@interface EventViewController ()

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) cUIScrollView *scrollView;

@end

@implementation EventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initEventDetailScrollView];
    [self initEventDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.tintColor = nil;
    self.navigationController.navigationBar.translucent = NO;
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.hidesBackButton = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    self.navigationItem.titleView = nil;
    self.title = @"Events";
}

-(void)initEventDetailScrollView{
    
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    _scrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _screenHeight * 2);
    _scrollView.delaysContentTouches = NO;
    [self.view addSubview:_scrollView];
}

-(void)initEventDetails{
    
    UIView *eventView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, _screenHeight / 3)];
    //featureView.backgroundColor = [UIColor orangeColor];
    [_scrollView addSubview:eventView];
    
    UIImageView *eventImageView = [[UIImageView alloc] init];
    eventImageView.frame = CGRectMake(0, 0, eventView.frame.size.width, eventView.frame.size.height);
    //_eventImageView.backgroundColor = [UIColor orangeColor];
    //eventImageView.image = [UIImage imageNamed:_eventImageString];
    eventImageView.contentMode = UIViewContentModeScaleAspectFill;
    eventImageView.clipsToBounds = YES;
    [eventView addSubview:eventImageView];
    
    ImageLoader *il = [[ImageLoader alloc] init];
    [il parseImage:eventImageView url:_imageUrl];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, eventImageView.frame.size.height, eventImageView.frame.size.width, 60)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.layer.masksToBounds = NO;
    headerView.layer.shadowOffset = CGSizeMake(-2, 2);
    headerView.layer.shadowRadius = 5;
    headerView.layer.shadowOpacity = 0.2;
    [_scrollView addSubview:headerView];
    
    UILabel *titleHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, headerView.frame.size.width - 60, headerView.frame.size.height)];
    titleHeaderLabel.text = _eventTitle;
    titleHeaderLabel.textAlignment = NSTextAlignmentCenter;
    titleHeaderLabel.numberOfLines = 0;
    titleHeaderLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleHeaderLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
    titleHeaderLabel.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    [headerView addSubview:titleHeaderLabel];
    
    UIView *detailsView = [[UIView alloc] initWithFrame:CGRectMake(0, headerView.frame.origin.y + headerView.frame.size.height, headerView.frame.size.width, 100)];
    //detailsView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
    [_scrollView addSubview:detailsView];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, detailsView.frame.size.width - 30, detailsView.frame.size.height)];
    //descriptionLabel.backgroundColor = [UIColor greenColor];
    descriptionLabel.text = [NSString stringWithFormat:@"%@ fnsjdkfnsjknvkjsdnkfjnsdjkvkjsn kjnksjn ksn s kbsd kjbskjd bksjb jksbd jkbskdj nskjn kjsnd kjsnjk nskdj nskjdn jksdb jksdb kjsbkj bskjd jksdn jksn jkbdjhb shjdb shjdb hjsdb hjsdb jhsdb hjsdb sbd hsb hsdb sdb hsbd hbsjhdb hbsd hsjhb jfhsb hsj fnsjdkfnsjknvkjsdnkfjnsdjkvkjsn kjnksjn ksn s kbsd kjbskjd bksjb jksbd jkbskdj nskjn kjsnd kjsnjk nskdj nskjdn jksdb jksdb kjsbkj bskjd jksdn jksn jkbdjhb shjdb shjdb hjsdb hjsdb jhsdb hjsdb sbd hsb hsdb sdb hsbd hbsjhdb hbsd hsjhb jfhsb hsjfnsjdkfnsjknvkjsdnkfjnsdjkvkjsn kjnksjn ksn s kbsd kjbskjd bksjb jksbd jkbskdj nskjn kjsnd kjsnjk nskdj nskjdn jksdb jksdb kjsbkj bskjd jksdn jksn jkbdjhb shjdb shjdb hjsdb hjsdb jhsdb hjsdb sbd hsb hsdb sdb hsbd hbsjhdb hbsd hsjhb jfhsb hsj",_eventDescription];
    descriptionLabel.numberOfLines = 0;
    descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    descriptionLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    descriptionLabel.textColor = [UIColor darkGrayColor];
    [descriptionLabel sizeToFit];
    descriptionLabel.frame = descriptionLabel.frame;
    [detailsView addSubview:descriptionLabel];
    
    detailsView.frame = CGRectMake(detailsView.frame.origin.x, detailsView.frame.origin.y, detailsView.frame.size.width, descriptionLabel.frame.size.height);
    
    UIButton *bookButton = [UIButton buttonWithType:UIButtonTypeSystem];
    bookButton.frame = CGRectMake(_screenWidth / 4, detailsView.frame.origin.y + detailsView.frame.size.height + 30, _screenWidth / 2, 40);
    bookButton.backgroundColor = [UIColor colorWithRed:21/255.0f green:115/255.0f blue:186/255.0f alpha:1.0];
    bookButton.layer.cornerRadius = 10;
    [bookButton setTitle:@"Book This Class" forState:UIControlStateNormal];
    [bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_scrollView addSubview:bookButton];
    
    [bookButton addTarget:self action:@selector(bookEvent) forControlEvents:UIControlEventTouchUpInside];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, bookButton.frame.origin.y + (bookButton.frame.size.height * 2) + self.navigationController.navigationBar.frame.size.height);
}

-(void)bookEvent{
    
    BookingViewController *bookingVC = [[BookingViewController alloc] init];
    bookingVC.eventId = _eventId;
    bookingVC.imageUrl = _imageUrl;
    bookingVC.eventTitle = _eventTitle;
    [self.navigationController pushViewController:bookingVC animated:YES];
    NSLog(@"Book This Event");
}
@end
