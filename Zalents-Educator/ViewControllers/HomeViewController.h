//
//  HomeViewController.h
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 7/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController <UIScrollViewDelegate>

@end
