//
//  HomeViewController.m
//  Zalents-Educator
//
//  Created by LF-Mac-Air on 7/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "HomeViewController.h"
#import "MenuController.h"
#import "EventViewController.h"

#import "cUIScrollView.h"
#import "FeatureView.h"
#import "EventView.h"

#import "JsonParser.h"
#import "Events.h"

#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)

@interface HomeViewController () <MenuControllerDelegate, FeatureViewDelegate, EventViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) MenuController *mc;

//@property (nonatomic, strong) NSArray *imagesArray;

@property (nonatomic, strong) cUIScrollView *scrollView;
@property (nonatomic, strong) UIScrollView *pagerScrollView;
@property (nonatomic, strong) UIView *spaceView;

@property (nonatomic) NSInteger featureCounter;
@property (nonatomic) NSInteger eventsCounter;

@property (nonatomic, strong) NSMutableArray *eventIdArray;
@property (nonatomic, strong) NSMutableArray *imageMediumUrlArray;
@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) NSMutableArray *creditArray;
@property (nonatomic, strong) NSMutableArray *descriptionArray;

@property (nonatomic, strong) UIView *featureDetailView;
@property (nonatomic, strong) UILabel *featureTitleLabel;
//@property (nonatomic, strong) UILabel *featureDescriptionLabel;

/*
 * Dummy data
 */

//@property (nonatomic) NSInteger fakeFeaturesCounter;
//@property (nonatomic) NSInteger fakeAllEventsCounter;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initHomeScrollView];
    [self initFeatureLayout];
    //[self initEventsLayout];
    [self initMenuController];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height - (self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height);
    
    //_imagesArray = @[@"january.jpg", @"february.jpg", @"march.jpg", @"april.jpg", @"may.jpg", @"june.jpg",
    //                @"july.jpg", @"august.jpg", @"september.jpg", @"october.jpg", @"november.jpg", @"december.jpg"];

    //_fakeFeaturesCounter = _imagesArray.count / 2;
    //_fakeAllEventsCounter = _imagesArray.count;
    
    JsonParser *json = [[JsonParser alloc] init];

    _eventIdArray = [[NSMutableArray alloc] init];
    _imageMediumUrlArray = [[NSMutableArray alloc] init];
    _titleArray = [[NSMutableArray alloc] init];
    _creditArray = [[NSMutableArray alloc] init];
    _descriptionArray = [[NSMutableArray alloc] init];
    
    [json parseJsonURL:@"http://testedu.everythingdemo.com/api/v1/events.json" parsing:^(BOOL isComplete){
        
        if(isComplete){
            
            for (int i = 0; i < json.jsonArray.count; i++) {
                
                Events *events = [[Events alloc] initWithDictionary:[json.jsonArray objectAtIndex:i]];
                
                [_eventIdArray addObject:events.eventsId];
                [_imageMediumUrlArray addObject:events.eventsImageMediumUrl];
                [_titleArray addObject:events.eventsTitle];
                [_creditArray addObject:events.eventsCredit];
                [_descriptionArray addObject:events.eventsDescription];
            }
            
            _featureCounter = _titleArray.count / 2;
            _eventsCounter = _titleArray.count;
            
            _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 80 + (((_screenHeight / 3) + 60) * (_eventsCounter + 1)));
            
            _featureTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, _featureDetailView.frame.size.width - 20, 40)];
            //_titleLabel.backgroundColor = [UIColor greenColor];
            _featureTitleLabel.text = [_titleArray objectAtIndex:_pageControl.currentPage];
            _featureTitleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:25];
            _featureTitleLabel.textColor = [UIColor whiteColor];
            [_featureDetailView addSubview:_featureTitleLabel];
            
//            _featureDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, _featureTitleLabel.frame.origin.y + _featureTitleLabel.frame.size.height, _featureDetailView.frame.size.width - 20, 20)];
//            //_titleLabel.backgroundColor = [UIColor greenColor];
//            _featureDescriptionLabel.text = [_descriptionArray objectAtIndex:_pageControl.currentPage];
//            _featureDescriptionLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16];
//            _featureDescriptionLabel.textColor = [UIColor whiteColor];
//            [_featureDetailView addSubview:_featureDescriptionLabel];
            
            _pageControl.numberOfPages = _featureCounter;
            
            _pagerScrollView.contentSize = CGSizeMake(_pagerScrollView.frame.size.width * _featureCounter, _pagerScrollView.frame.size.height);
            
            for (int i = 0; i < _featureCounter; i++) {
                
                FeatureView *featureView = [[FeatureView alloc] initWithFrame:CGRectMake(_pagerScrollView.frame.size.width * i, 0, _pagerScrollView.frame.size.width, _pagerScrollView.frame.size.height)];
                //featureView.backgroundColor = [UIColor orangeColor];
                featureView.delegate = self;
                featureView.imageUrl = [NSString stringWithFormat:@"%@",[_imageMediumUrlArray objectAtIndex:i]];
                featureView.tag = i;
                [_pagerScrollView addSubview:featureView];
            }
            
            [self initEventsLayout];
            
            NSLog(@"page counter = %ld", (long)_pageControl.numberOfPages);
            NSLog(@"complete");
        }else{
            
            NSLog(@"failed");
        }
    }];
}

-(void)initHomeScrollView{
    
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    _scrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    //_scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 80 + (((_screenHeight / 3) + 60) * (_eventsCounter + 1)));
    _scrollView.delaysContentTouches = NO;
    [self.view addSubview:_scrollView];
}

-(void)initFeatureLayout{
    
    _pagerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, (_screenHeight / 3) + 60)];
    //pagerScrollView.backgroundColor = [UIColor greenColor];
    _pagerScrollView.delegate = self;
    _pagerScrollView.bounces = NO;
    _pagerScrollView.pagingEnabled = YES;
    //_pagerScrollView.contentSize = CGSizeMake(_pagerScrollView.frame.size.width * _featureCounter, _pagerScrollView.frame.size.height);
    _pagerScrollView.showsHorizontalScrollIndicator = NO;
    [_scrollView addSubview:_pagerScrollView];
    
    /*
    for (int i = 0; i < _featureCounter; i++) {
        
        FeatureView *featureView = [[FeatureView alloc] initWithFrame:CGRectMake(_pagerScrollView.frame.size.width * i, 0, _pagerScrollView.frame.size.width, _pagerScrollView.frame.size.height)];
        //featureView.backgroundColor = [UIColor orangeColor];
        featureView.delegate = self;
        featureView.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[_imageMediumUrlArray objectAtIndex:i]]];
        featureView.tag = i;
        [_pagerScrollView addSubview:featureView];
    }
    */
    
    UIImage *featureBannerimage = [UIImage imageNamed:@"featureBanner150px"];
    
    UIImageView *featureBannerImageView = [[UIImageView alloc] initWithImage:featureBannerimage];
    featureBannerImageView.frame = CGRectMake(0, 0, featureBannerimage.size.width, featureBannerimage.size.height);
    //featureImageView.backgroundColor = [UIColor orangeColor];
    featureBannerImageView.contentMode = UIViewContentModeScaleAspectFit;
    featureBannerImageView.clipsToBounds = YES;
    [_scrollView addSubview:featureBannerImageView];
    
    _featureDetailView = [[UIView alloc] initWithFrame:CGRectMake(0, _pagerScrollView.frame.size.height - 60, _pagerScrollView.frame.size.width, 40)];
    _featureDetailView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [_scrollView addSubview:_featureDetailView];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, _pagerScrollView.frame.size.height - 20, _screenWidth, 20)];
    _pageControl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    _pageControl.numberOfPages = _featureCounter;
    _pageControl.autoresizingMask = UIViewAutoresizingNone;
    [_scrollView addSubview:_pageControl];
    
    _spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, _pagerScrollView.frame.origin.y + _pagerScrollView.frame.size.height, _screenWidth, 20)];
    _spaceView.backgroundColor = [UIColor whiteColor];
    _spaceView.layer.masksToBounds = NO;
    _spaceView.layer.shadowOffset = CGSizeMake(-2, 2);
    _spaceView.layer.shadowRadius = 5;
    _spaceView.layer.shadowOpacity = 0.5;
    [_scrollView addSubview:_spaceView];
}

-(void)initEventsLayout{
    
    for (int i = 0; i < _eventsCounter; i++) {
        
        EventView *eventView = [[EventView alloc] initWithFrame:CGRectMake(0, (((_screenHeight / 3) + 60) * i) + _pagerScrollView.frame.size.height + _spaceView.frame.size.height, _pagerScrollView.frame.size.width, (_screenHeight / 3) + 60)];
        //eventView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.3];
        eventView.delegate = self;
        eventView.imageUrl = [NSString stringWithFormat:@"%@",[_imageMediumUrlArray objectAtIndex:i]];
        eventView.tag = i;
        eventView.title = [_titleArray objectAtIndex:i];
        eventView.descriptionEvent = [_descriptionArray objectAtIndex:i];
        eventView.price = [_creditArray objectAtIndex:i];
        eventView.layer.cornerRadius = 10;
        [_scrollView addSubview:eventView];
    }
}

-(void)initMenuController{
    
    _mc = [[MenuController alloc] initWithFrame:CGRectMake(-(_screenWidth - (_screenWidth / 4)), 0, _screenWidth - (_screenWidth / 4), _screenHeight)];
    _mc.delegate = self;
    _mc.popToFront = NO;
    //_mc.headerSpace = 130;
    //_mc.menu.settings.textColor = [UIColor colorWithRed:5/255.f green:11/255.f blue:103/255.f alpha:1.0];
    //_mc.menu.settings.menuBackgroundColor = [UIColor whiteColor];
    [self.view addSubview:_mc];
    
    NSLog(@"list from matrix = %@",_mc.matrixToList);
}

/*
 * Delegates
 */

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    _pageControl.currentPage = fabs(((scrollView.contentSize.width - scrollView.contentOffset.x) / scrollView.frame.size.width) - _featureCounter);
    
    /*
     * Fade-in and Fade-out animation
     */
     
    NSString *nextTitle = [_titleArray objectAtIndex:_pageControl.currentPage];
    //NSString *nextDescription = [_descriptionArray objectAtIndex:_pageControl.currentPage];
    
    [UIView transitionWithView:_featureTitleLabel
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _featureTitleLabel.text = nextTitle;
                    }completion:nil];
//    
//    [UIView transitionWithView:_featureDescriptionLabel
//                      duration:0.5f
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^{
//                        _featureDescriptionLabel.text = nextDescription;
//                    }completion:nil];
}

-(void)didSelectFeature:(UIButton *)sender{
    
    EventViewController *eventVC = [[EventViewController alloc] init];
    eventVC.eventId = [_eventIdArray objectAtIndex:sender.tag];
    eventVC.imageUrl = [_imageMediumUrlArray objectAtIndex:sender.tag];
    eventVC.eventTitle = [_titleArray objectAtIndex:sender.tag];
    eventVC.eventDescription = [_descriptionArray objectAtIndex:sender.tag];
    [self.navigationController pushViewController:eventVC animated:YES];
}

-(void)didSelectEvent:(UIButton *)sender{
    
    EventViewController *eventVC = [[EventViewController alloc] init];
    eventVC.eventId = [_eventIdArray objectAtIndex:sender.tag];
    eventVC.imageUrl = [_imageMediumUrlArray objectAtIndex:sender.tag];
    eventVC.eventTitle = [_titleArray objectAtIndex:sender.tag];
    eventVC.eventDescription = [_descriptionArray objectAtIndex:sender.tag];
    [self.navigationController pushViewController:eventVC animated:YES];
}

-(void)didSelectMenu:(MenuController *)menuController viewController:(UIViewController *)viewController{
    
    NSLog(@"viewController = %@",viewController);
    
    if([self.title isEqualToString:viewController.title]){
        NSLog(@"I'am clicking the view controller that is already presented");
    }else{
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(void)didSelectSubMenu:(MenuController *)menuController{
    NSLog(@"Did select sub menu");
}
@end
