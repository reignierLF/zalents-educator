//
//  ListEventViewController.h
//  Zalents-Educator
//
//  Created by Livefitter on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListEventViewController : UIViewController<UIScrollViewDelegate>

@end
