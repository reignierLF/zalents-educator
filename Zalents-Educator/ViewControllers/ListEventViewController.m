//
//  ListEventViewController.m
//  Zalents-Educator
//
//  Created by Livefitter on 27/9/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ListEventViewController.h"
#import "JsonParser.h"
#import "MenuController.h"
#import "Menu.h"
#import "cUIScrollView.h"
#import "ListEventView.h"

#import "FeatureView.h"

#import "Events.h"

#import "TimeConverter.h"
#import "EventViewController.h"

#import "ListEventView.h"
#import "EventView.h"


@interface ListEventViewController ()<MenuControllerDelegate, ListEventViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic, strong) MenuController *mc;


@property (nonatomic, strong) UIScrollView *pagerScrollView;
@property (nonatomic, strong) cUIScrollView *scrollView;
@property (nonatomic, strong) UIView *spaceView;

@property (nonatomic) NSInteger fakeAllEventsCounter;

@property (nonatomic) NSInteger featureCounter;
@property (nonatomic) NSInteger eventsCounter;

@property (nonatomic, strong) NSMutableArray *imageMediumUrlArray;
@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) NSMutableArray *descriptionArray;
@property (nonatomic, strong) NSMutableArray *eventIdArray;
@property (nonatomic, strong) NSMutableArray *dateArray;
@property (nonatomic, strong) NSMutableArray *addressArray;

@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) UISegmentedControl *segment;
@property (nonatomic) int segmentIndex;




@end

@implementation ListEventViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    _segmentIndex = 0;
    
    _url = @"http://testedu.everythingdemo.com/api/v1/events.json";
    
    [self initialize];
    [self initHomeScrollView];
    [self initFeatureLayout];
    [self initMenuController];
    
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.translucent = NO;
    

}

-(void)initialize{
    
    TimeConverter *tm = [[TimeConverter alloc]init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height - (self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height);
    
    
    JsonParser *json = [[JsonParser alloc] init];
    
    _imageMediumUrlArray = [[NSMutableArray alloc] init];
    _titleArray = [[NSMutableArray alloc] init];
    
    _eventIdArray = [[NSMutableArray alloc] init];
    _descriptionArray = [[NSMutableArray alloc] init];
    
    _dateArray = [[NSMutableArray alloc] init];
    _addressArray = [[NSMutableArray alloc] init];
    
    [json parseJsonURL:_url parsing:^(BOOL isComplete){
        
        if(isComplete){
            
            for (int i = 0; i < json.jsonArray.count; i++) {
                
                Events *events = [[Events alloc] initWithDictionary:[json.jsonArray objectAtIndex:i]];
                
                [_imageMediumUrlArray addObject:events.eventsImageMediumUrl];
                [_eventIdArray addObject:events.eventsId];
                [_titleArray addObject:events.eventsTitle];
                [_descriptionArray addObject:events.eventsDescription];
                
                
                NSLog(@" addressArray %@",_dateArray);
                
                if (events.eventBatches.eventBatchesVenue.count ==0) {
                    [_addressArray addObject:@""];
                    [_dateArray addObject:@""];
                }else{
                    
                    NSString *date;
                    if ([events.eventBatches.eventBatchesEndAt objectAtIndex:0] == (id)[NSNull null]) {
                        date = @"";
                    }else{
                        date = [tm serverDateFormatToDate:[events.eventBatches.eventBatchesEndAt objectAtIndex:0]];
                    }
                    
                    NSString *startTime;
                    if ([events.eventBatches.eventBatchesStartTime objectAtIndex:0] == (id)[NSNull null]) {
                        startTime = @"";
                    }else{
                        startTime = [events.eventBatches.eventBatchesStartTime objectAtIndex:0];
                    }
                    
                    NSString *endTime;
                    if ([events.eventBatches.eventBatchesStartTime objectAtIndex:0] == (id)[NSNull null]) {
                        endTime = @"";
                    }else{
                        endTime = [events.eventBatches.eventBatchesStartTime objectAtIndex:0];
                    }
                    
                    NSString *time;
                    if ([startTime isEqualToString:@""]) {
                        time = @"";
                    }else{
                        time = [NSString stringWithFormat:@", %@-%@",startTime,endTime];
                    }
                    [_dateArray addObject:[NSString stringWithFormat:@"%@%@",date,time]];
                    
                    
                    if ([[events.eventBatches.eventBatchesVenue objectAtIndex:0] isEqual:[NSNull null]]) {
                         [_addressArray addObject:@""];
                    }else{
                        [_addressArray addObject:[events.eventBatches.eventBatchesVenue objectAtIndex:0]];
                    }
                    
                }
            }
            
            _featureCounter = _titleArray.count / 2;
            _eventsCounter = _titleArray.count;
            
            _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 80 + (((_screenHeight / 3) + 60) * (_eventsCounter + 1)));
            
            _pageControl.numberOfPages = _featureCounter;
            
            _pagerScrollView.contentSize = CGSizeMake(_pagerScrollView.frame.size.width * _featureCounter, _pagerScrollView.frame.size.height);
            
//            for (int i = 0; i < _featureCounter; i++) {
//                
//                FeatureView *featureView = [[FeatureView alloc] initWithFrame:CGRectMake(_pagerScrollView.frame.size.width * i, 0, _pagerScrollView.frame.size.width, _pagerScrollView.frame.size.height)];
//                //featureView.backgroundColor = [UIColor orangeColor];
//                featureView.delegate = self;
//                featureView.imageUrl = [NSString stringWithFormat:@"%@",[_imageMediumUrlArray objectAtIndex:i]];
//                featureView.tag = i;
//                [_pagerScrollView addSubview:featureView];
//            }
            
            [self initEventsLayout];
            
            NSLog(@"page counter = %ld", (long)_pageControl.numberOfPages);
            NSLog(@"complete");
        }else{
            
            NSLog(@"failed");
        }
    }];

    
    
    
}
-(void)initMenuController{
    
    _mc = [[MenuController alloc] initWithFrame:CGRectMake(-(_screenWidth - (_screenWidth / 4)), 0, _screenWidth - (_screenWidth / 4), _screenHeight+70)];
    _mc.delegate = self;
    
    //_mc.popToFront = YES;
    //_mc.headerSpace = 130;
    //_mc.menu.settings.textColor = [UIColor colorWithRed:5/255.f green:11/255.f blue:103/255.f alpha:1.0];
    //_mc.menu.settings.menuBackgroundColor = [UIColor greenColor];
    [self.view addSubview:_mc];
    //UIView *testvie
    self.navigationItem.titleView = nil;
    
    NSLog(@"list from matrix = %@",_mc.matrixToList);
}
-(void)didSelectMenu:(MenuController *)menuController viewController:(UIViewController *)viewController{
    
    NSLog(@"viewController = %@",viewController);
    
    if([self.title isEqualToString:viewController.title]){
        NSLog(@"I'am clicking the view controller that is already presented");
    }else{
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(void)didSelectSubMenu:(MenuController *)menuController{
    NSLog(@"Did select sub menu");
}
-(void)initHomeScrollView{
   
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    _scrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 80 + (((_screenHeight / 3) + 60) * (_fakeAllEventsCounter + 1)));
    _scrollView.delaysContentTouches = NO;
    [self.view addSubview:_scrollView];
}
-(void)initHomeScrollView2{

    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight + 70)];
    _scrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 80 + (((_screenHeight / 3) + 60) * (_fakeAllEventsCounter + 1)));
    _scrollView.delaysContentTouches = NO;
    [self.view addSubview:_scrollView];
}

-(void)initFeatureLayout{
    
    _pagerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, (_screenHeight / 3) + 60)];
    //pagerScrollView.backgroundColor = [UIColor greenColor];
    _pagerScrollView.delegate = self;
    _pagerScrollView.bounces = NO;
    _pagerScrollView.pagingEnabled = YES;
    //_pagerScrollView.contentSize = CGSizeMake(_pagerScrollView.frame.size.width * _featureCounter, _pagerScrollView.frame.size.height);
    _pagerScrollView.showsHorizontalScrollIndicator = NO;
    //[_scrollView addSubview:_pagerScrollView];
    
    /*
     for (int i = 0; i < _featureCounter; i++) {
     
     FeatureView *featureView = [[FeatureView alloc] initWithFrame:CGRectMake(_pagerScrollView.frame.size.width * i, 0, _pagerScrollView.frame.size.width, _pagerScrollView.frame.size.height)];
     //featureView.backgroundColor = [UIColor orangeColor];
     featureView.delegate = self;
     featureView.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[_imageMediumUrlArray objectAtIndex:i]]];
     featureView.tag = i;
     [_pagerScrollView addSubview:featureView];
     }
     */
    
    UIImage *featureBannerimage = [UIImage imageNamed:@"featureBanner150px"];
    
    UIImageView *featureBannerImageView = [[UIImageView alloc] initWithImage:featureBannerimage];
    featureBannerImageView.frame = CGRectMake(0, 0, featureBannerimage.size.width, featureBannerimage.size.height);
    //featureImageView.backgroundColor = [UIColor orangeColor];
    featureBannerImageView.contentMode = UIViewContentModeScaleAspectFit;
    featureBannerImageView.clipsToBounds = YES;
    //[_scrollView addSubview:featureBannerImageView];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, _pagerScrollView.frame.size.height - 30, _screenWidth, 30)];
    _pageControl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    _pageControl.numberOfPages = _featureCounter;
    _pageControl.autoresizingMask = UIViewAutoresizingNone;
    //[_scrollView addSubview:_pageControl];
    
    _spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, _pagerScrollView.frame.origin.y + _pagerScrollView.frame.size.height, _screenWidth, 50)];
    _spaceView.backgroundColor = [UIColor whiteColor];
    //[_scrollView addSubview:_spaceView];
    
    NSArray *itemArray = [NSArray arrayWithObjects: @"Upcoming Events", @"Past Events", nil];
    _segment = [[UISegmentedControl alloc] initWithItems:itemArray];
    _segment.selectedSegmentIndex = _segmentIndex;
    _segment.frame = CGRectMake(20, 20, _screenWidth-40, 40);
    [_segment addTarget:self action:@selector(segmentAction:) forControlEvents: UIControlEventValueChanged];
    [_scrollView addSubview:_segment];
    
    
}

- (void)segmentAction:(UISegmentedControl *)segment
{
    [_scrollView removeFromSuperview];
    _scrollView = nil;
    
    
    if(segment.selectedSegmentIndex == 0)
    {
        _segmentIndex = 0;
        NSLog(@"Upcoming Events");
        _url = @"http://testedu.everythingdemo.com/api/v1/events.json";
        
    }
    else if(segment.selectedSegmentIndex == 1)
    {
        _segmentIndex = 1;
        NSLog(@"Past Events");
        _url = @"http://testedu.everythingdemo.com/api/v1/events.json";
    }
    



    [self initialize];
    [self initHomeScrollView2];
    [self initFeatureLayout];
    [self initMenuController];

   
}

-(void)initEventsLayout{
    
    for (int i = 0; i < _eventsCounter; i++) {
        
        ListEventView *eventView = [[ListEventView alloc] initWithFrame:CGRectMake(0, (((_screenHeight / 3) + 80) * i) + _spaceView.frame.size.height +20, _pagerScrollView.frame.size.width, (_screenHeight / 3) + 120)];
        [eventView removeFromSuperview];
        //eventView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.3];
        eventView.delegate = self;
        eventView.imageUrl = [NSString stringWithFormat:@"%@",[_imageMediumUrlArray objectAtIndex:i]];
        eventView.tag = i;
        eventView.title = [_titleArray objectAtIndex:i];
        eventView.date = [_dateArray objectAtIndex:i];
        eventView.address = [_addressArray objectAtIndex:i];
        [_scrollView addSubview:eventView];
    }
}
-(void)didSelectListEvent:(UIButton *)sender{

    EventViewController *eventVC = [[EventViewController alloc] init];
    eventVC.eventId = [_eventIdArray objectAtIndex:sender.tag];
    eventVC.imageUrl = [_imageMediumUrlArray objectAtIndex:sender.tag];
    eventVC.eventTitle = [_titleArray objectAtIndex:sender.tag];
    eventVC.eventDescription = [_descriptionArray objectAtIndex:sender.tag];
    [self.navigationController pushViewController:eventVC animated:YES];

}


@end
