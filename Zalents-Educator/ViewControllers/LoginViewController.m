//
//  LoginViewController.m
//  Zalents
//
//  Created by LF-Mac-Air on 6/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "LoginViewController.h"

#import "FacebookHandler.h"
#import "HomeViewController.h"
#import "RegistrationViewController.h"
//#import "ForgotPasswordViewController.h"
#import "ZalentsApi.h"

#import "cUITextField.h"

@interface LoginViewController () <UITextFieldDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) FacebookHandler *fbHandler;
@property (nonatomic, strong) ZalentsApi *zalentsApi;

@property (nonatomic, strong) UIImageView *welcomeImageView;

@property (nonatomic, strong) cUITextField *emailTextField;
@property (nonatomic, strong) cUITextField *passwordTextField;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
    [self initLoginLayout];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.title = @"";
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    
    _fbHandler = [[FacebookHandler alloc] init];
    _zalentsApi = [[ZalentsApi alloc] init];
}

-(void)initBackgroundImage{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login"]];
    backgroundImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
}

-(void)initLoginLayout{
    
    UIImageView *zalentsTitleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zalents logo big"]];
    zalentsTitleImageView.frame = CGRectMake((_screenWidth / 2) - ((_screenWidth - 60) / 2), 110, _screenWidth - 60, 100);
    //zalentsTitleImageView.backgroundColor = [UIColor orangeColor];
    zalentsTitleImageView.contentMode = UIViewContentModeScaleAspectFit;
    zalentsTitleImageView.clipsToBounds = YES;
    [self.view addSubview:zalentsTitleImageView];
    
    _emailTextField = [[cUITextField alloc] initWithFrame:CGRectMake(30, zalentsTitleImageView.frame.origin.y + zalentsTitleImageView.frame.size.height + 20, zalentsTitleImageView.frame.size.width, 40)];
    _emailTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _emailTextField.delegate = self;
    _emailTextField.text = @"Email Address";
    _emailTextField.textColor = [UIColor grayColor];
    _emailTextField.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    _emailTextField.textAlignment = NSTextAlignmentLeft;
    _emailTextField.layer.cornerRadius = 6;
    [self.view addSubview:_emailTextField];
    
    _passwordTextField = [[cUITextField alloc] initWithFrame:CGRectMake(_emailTextField.frame.origin.x, _emailTextField.frame.origin.y + _emailTextField.frame.size.height + 10, _emailTextField.frame.size.width, _emailTextField.frame.size.height)];
    _passwordTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _passwordTextField.delegate = self;
    _passwordTextField.text = @"Password";
    _passwordTextField.textColor = [UIColor grayColor];
    _passwordTextField.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    _passwordTextField.textAlignment = NSTextAlignmentLeft;
    _passwordTextField.layer.cornerRadius = 6;
    [self.view addSubview:_passwordTextField];
    
    UIButton *forgotPasswordButton = [UIButton buttonWithType:UIButtonTypeSystem];
    forgotPasswordButton.frame = CGRectMake(_passwordTextField.frame.origin.x, _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height + 2, _passwordTextField.frame.size.width, 10);
    forgotPasswordButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [forgotPasswordButton setTitle:@"Forgot your password?" forState:UIControlStateNormal];
    [forgotPasswordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    float newForgotPasswordButtonWidth = forgotPasswordButton.titleLabel.intrinsicContentSize.width;
    
    forgotPasswordButton.frame = CGRectMake(_screenWidth - (newForgotPasswordButtonWidth + 30), forgotPasswordButton.frame.origin.y, newForgotPasswordButtonWidth, forgotPasswordButton.frame.size.height);
    [self.view addSubview:forgotPasswordButton];
    
    [forgotPasswordButton addTarget:self action:@selector(loadForgotPasswordPage) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *loginImage = [UIImage imageNamed:@"loginButton"];
    float loginImageWidth = loginImage.size.width - (loginImage.size.width * 0.2);
    float loginImageHeight = loginImage.size.height - (loginImage.size.height * 0.2);
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    loginButton.frame = CGRectMake((_screenWidth / 2) - (loginImageWidth / 2), _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height + 30, loginImageWidth, loginImageHeight);
    [loginButton setImage:loginImage forState:UIControlStateNormal];
    loginButton.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:loginButton];
    
    [loginButton addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *orImage = [UIImage imageNamed:@"or"];
    float orImageWidth = orImage.size.width - (orImage.size.width * 0.2);
    float orImageHeight = orImage.size.height - (orImage.size.height * 0.2);
    
    UIImageView *orImageView = [[UIImageView alloc] initWithImage:orImage];
    orImageView.frame = CGRectMake(loginButton.frame.origin.x, loginButton.frame.origin.y + loginButton.frame.size.height + 10, orImageWidth, orImageHeight);
    //orImageView.backgroundColor = [UIColor orangeColor];
    orImageView.contentMode = UIViewContentModeScaleAspectFit;
    orImageView.clipsToBounds = YES;
    [self.view addSubview:orImageView];
    
    UIImage *fbLoginImage = [UIImage imageNamed:@"fb-loginButton"];
    float fbLoginImageWidth = fbLoginImage.size.width - (fbLoginImage.size.width * 0.2);
    float fbLoginImageHeight = fbLoginImage.size.height - (fbLoginImage.size.height * 0.2);
    
    UIButton *fbLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    fbLoginButton.frame = CGRectMake((_screenWidth / 2) - (fbLoginImageWidth / 2), orImageView.frame.origin.y + orImageView.frame.size.height + 10, fbLoginImageWidth, fbLoginImageHeight);
    [fbLoginButton setImage:fbLoginImage forState:UIControlStateNormal];
    fbLoginButton.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:fbLoginButton];
    
    [fbLoginButton addTarget:self action:@selector(fbLoginEvent) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *signUpLabel = [[UILabel alloc] initWithFrame:CGRectMake(_passwordTextField.frame.origin.x, fbLoginButton.frame.origin.y + fbLoginButton.frame.size.height + 5, 100, 10)];
    signUpLabel.text = @"Don't have an account?";
    signUpLabel.textColor = [UIColor whiteColor];
    signUpLabel.font = [UIFont systemFontOfSize:14];
    
    float newSignUpLabelWidth = signUpLabel.intrinsicContentSize.width;
    
    UIButton *signUpButton = [UIButton buttonWithType:UIButtonTypeSystem];
    signUpButton.frame = CGRectMake(_passwordTextField.frame.origin.x, _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height + 2, _passwordTextField.frame.size.width, 10);
    signUpButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [signUpButton setTitle:@"Sign Up?" forState:UIControlStateNormal];
    [signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    float newSignUpButtonWidth = signUpButton.titleLabel.intrinsicContentSize.width;
    
    signUpLabel.frame = CGRectMake((_screenWidth / 2) - ((newSignUpLabelWidth + newSignUpButtonWidth) / 2), self.view.frame.size.height - 40, newSignUpLabelWidth, signUpLabel.frame.size.height);
    [self.view addSubview:signUpLabel];
    
    signUpButton.frame = CGRectMake(signUpLabel.frame.origin.x + signUpLabel.frame.size.width + 2, signUpLabel.frame.origin.y, newSignUpButtonWidth, signUpLabel.frame.size.height);
    [self.view addSubview:signUpButton];
    
    [signUpButton addTarget:self action:@selector(loadSignUpPage) forControlEvents:UIControlEventTouchUpInside];
}

/*
 * Events
 */

-(void)fbLoginEvent{
    
    [_fbHandler fbLoginEvent:^(BOOL isComplete){
        
        if(isComplete){
            
            [_zalentsApi login:@"http://testedu.everythingdemo.com"
                        username:_emailTextField.text
                        password:_passwordTextField.text
                        isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse){
                
            }];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            NSLog(@"able to login");
        }else{
            
            NSLog(@"failed to login");
        }
    }];
}

-(void)login{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    NSLog(@"back to home");
}

-(void)loadForgotPasswordPage{
    
    NSLog(@"forgot password");
}

-(void)loadSignUpPage{
    
    RegistrationViewController *reg = [[RegistrationViewController alloc] init];
    [self.navigationController pushViewController:reg animated:YES];
}

-(void)emptyTextField:(UITextField *)textField{
    
    /*
     * Check if text field is empty or no inputs
     */
    
    if(textField.text.length == 0){
        
        /*
         * Check which text field
         *
         * If emailTextField is empty, "Email address" word
         * will be added inside the emailTextField, same goes
         * to passwordTextField
         */
        
        if(textField == _emailTextField){
            _emailTextField.text = @"Email address";
        }else if(textField == _passwordTextField){
            _passwordTextField.text = @"Password";
        }
    }
}

/*
 * Delegate
 */

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    /*
     * Check if passwordTextField is being click
     */
    
    if(textField == _passwordTextField){
        
        /*
         * Securing the password while typing
         * words/characters will be replace by *
         */
        
        _passwordTextField.secureTextEntry = YES;
        NSLog(@"password being typed but hidden");
    }
    
    /*
     * When the emailTextField/passwordTextField was click
     * remove any inputs in the text field
     */
    
    textField.text = @"";
    
    /*
     * Replace the "Return" key in the keyboard
     * with "Done" key
     * "Return" is the defauult key in the keyboard
     */
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    /*
     * If done editting to both text field
     * "emptyTextField" method is called to check
     * if their text field is empty
     */
    
    [self emptyTextField:textField];
    
    /*
     * Check if passwordTextField is being click
     */
    
    if(textField == _passwordTextField){
        
        /*
         * Detect if input is same to the word of "Password"
         * if YES/True, dont replace characters with *
         * if NO/False, replace all the characters with *
         */
        
        if([_passwordTextField.text isEqualToString:@"Password"]){
            _passwordTextField.secureTextEntry = NO;
        }else{
            _passwordTextField.secureTextEntry = YES;
        }
    }
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    /*
     * "emptyTextField" method is called after hitting "Done" key
     * to check if text field is empty
     */
    
    [self emptyTextField:textField];
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
    
    return YES;
}
@end
