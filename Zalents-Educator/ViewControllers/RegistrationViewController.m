//
//  RegistrationViewController.m
//  Zalents
//
//  Created by LF-Mac-Air on 6/8/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "RegistrationViewController.h"

#import "FacebookHandler.h"

#import "cUIScrollView.h"
#import "cUITextField.h"

#import "InterestView.h"
#import "ZalentsApi.h"


@interface RegistrationViewController () <UIScrollViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@property (nonatomic, strong) FacebookHandler *fbHandler;

@property (nonatomic, strong) UIImageView *welcomeImageView;
@property (nonatomic, strong) NSArray *headerTextArray;
@property (nonatomic) int counterNext;
@property (nonatomic, strong) UIScrollView *registrationPagerScrollView;
@property (nonatomic, strong) UILabel *registrationHeaderLabel;
@property (nonatomic, strong) UIView *firstView;
@property (nonatomic, strong) UIView *secondView;
@property (nonatomic, strong) UIView *thirdView;

@property (nonatomic, strong) cUITextField *firstNameTextField;
@property (nonatomic, strong) cUITextField *lastNameTextField;
@property (nonatomic, strong) cUITextField *emailTextField;
@property (nonatomic, strong) cUITextField *passwordTextField;
@property (nonatomic, strong) cUITextField *mobileNumberTextField;
@property (nonatomic, strong) cUITextField *countryTextField;
@property (nonatomic, strong) cUITextField *birthdayTextField;


@property (nonatomic, strong) NSArray *genderListArray;
@property (nonatomic, strong) UILabel *genderLabel;
@property (nonatomic, strong) UIView *backgroundGenderDropdown;
@property (nonatomic, strong) UIView *genderListView;
@property (nonatomic, strong) UIButton *nextButton;

@property (nonatomic, strong) ZalentsApi *zalentsApi;
@property (nonatomic) int *countPage;


@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initBackgroundImage];
    [self initRegistrationPagerScrollView];
    [self initFirstViewLayout:_firstView];
    [self initSecondViewLayout:_secondView];
    [self initThirdViewLayout:_thirdView];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;
    _countPage = 1 ;
    _counterNext = 0;
    
    
    _fbHandler = [[FacebookHandler alloc] init];
}

-(void)initBackgroundImage{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login"]];
    backgroundImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //backgroundImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
}

-(void)initRegistrationPagerScrollView{
    
    _headerTextArray = [[NSArray alloc] initWithObjects:
                                @"Let's get started!",
                                @"Now for finishing touch.",
                                @"Tell us what you're into.", nil];
    
    _registrationPagerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    //_registrationPagerScrollView.backgroundColor = [UIColor greenColor];
    _registrationPagerScrollView.delegate = self;
    _registrationPagerScrollView.bounces = NO;
    _registrationPagerScrollView.scrollEnabled = NO;
    _registrationPagerScrollView.pagingEnabled = YES;
    _registrationPagerScrollView.contentSize = CGSizeMake(_registrationPagerScrollView.frame.size.width * 3, _screenHeight);
    _registrationPagerScrollView.showsHorizontalScrollIndicator = YES;
    [self.view addSubview:_registrationPagerScrollView];
    
    _registrationHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 100, _screenWidth - 40, 40)];
    //registrationHeaderLabel.backgroundColor = [UIColor redColor];
    _registrationHeaderLabel.text = @"Let's get started!";
    _registrationHeaderLabel.textColor = [UIColor whiteColor];
    _registrationHeaderLabel.font = [UIFont boldSystemFontOfSize:22];
    _registrationHeaderLabel.textAlignment = NSTextAlignmentCenter;
    [_registrationHeaderLabel bringSubviewToFront:self.view];
    [self.view addSubview:_registrationHeaderLabel];
    
    _firstView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    //_firstView.backgroundColor = [UIColor blueColor];
    [_registrationPagerScrollView addSubview:_firstView];
    
    _secondView = [[UIView alloc] initWithFrame:CGRectMake(_screenWidth, 0, _screenWidth, _screenHeight)];
    //_secondView.backgroundColor = [UIColor blueColor];
    [_registrationPagerScrollView addSubview:_secondView];
    
    _thirdView = [[UIView alloc] initWithFrame:CGRectMake(_screenWidth * 2, 0, _screenWidth, _screenHeight)];
    //_thirdView.backgroundColor = [UIColor blueColor];
    [_registrationPagerScrollView addSubview:_thirdView];
    
    UILabel *signUpLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 20, _screenWidth, 10)];
    signUpLabel.text = @"By signing up you accept our Term of Use and Privacy Policy.";
    signUpLabel.textColor = [UIColor whiteColor];
    signUpLabel.font = [UIFont systemFontOfSize:8];
    signUpLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:signUpLabel];
}

-(void)initFirstViewLayout:(UIView*)view{
    
    UIView *inputContainerView = [[UIView alloc] initWithFrame:CGRectMake(20, _registrationHeaderLabel.frame.origin.y + _registrationHeaderLabel.frame.size.height + 10, _registrationHeaderLabel.frame.size.width, 45 * 4)];
    //inputContainerView.backgroundColor = [UIColor greenColor];
    inputContainerView.layer.cornerRadius = 6;
    [view addSubview:inputContainerView];
    
    _firstNameTextField = [[cUITextField alloc] initWithFrame:CGRectMake(0, 0, inputContainerView.frame.size.width, 40)];
    _firstNameTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _firstNameTextField.text = @"First Name";
    _firstNameTextField.textColor = [UIColor grayColor];
    _firstNameTextField.font = [UIFont systemFontOfSize:15];
    _firstNameTextField.textAlignment = NSTextAlignmentLeft;
    _firstNameTextField.layer.cornerRadius = 6;
    [inputContainerView addSubview:_firstNameTextField];
    
    _lastNameTextField = [[cUITextField alloc] initWithFrame:CGRectMake(0, _firstNameTextField.frame.origin.y + _firstNameTextField.frame.size.height + 5, inputContainerView.frame.size.width, 40)];
    _lastNameTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _lastNameTextField.text = @"Last Name";
    _lastNameTextField.textColor = [UIColor grayColor];
    _lastNameTextField.font = [UIFont systemFontOfSize:15];
    _lastNameTextField.textAlignment = NSTextAlignmentLeft;
    _lastNameTextField.layer.cornerRadius = 6;
    [inputContainerView addSubview:_lastNameTextField];
    
    _emailTextField = [[cUITextField alloc] initWithFrame:CGRectMake(0, _lastNameTextField.frame.origin.y + _lastNameTextField.frame.size.height + 5, inputContainerView.frame.size.width, 40)];
    _emailTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _emailTextField.text = @"Email Address";
    _emailTextField.textColor = [UIColor grayColor];
    _emailTextField.font = [UIFont systemFontOfSize:15];
    _emailTextField.textAlignment = NSTextAlignmentLeft;
    _emailTextField.layer.cornerRadius = 6;
    [inputContainerView addSubview:_emailTextField];
    
    _passwordTextField = [[cUITextField alloc] initWithFrame:CGRectMake(0, _emailTextField.frame.origin.y + _emailTextField.frame.size.height + 5, inputContainerView.frame.size.width, 40)];
    _passwordTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _passwordTextField.text = @"Password";
    _passwordTextField.textColor = [UIColor grayColor];
    _passwordTextField.font = [UIFont systemFontOfSize:15];
    _passwordTextField.textAlignment = NSTextAlignmentLeft;
    _passwordTextField.layer.cornerRadius = 6;
    [inputContainerView addSubview:_passwordTextField];
    
    UIImage *signupImage = [UIImage imageNamed:@"signupButton"];
    float signupImageWidth = signupImage.size.width - (signupImage.size.width * 0.2);
    float signupImageHeight = signupImage.size.height - (signupImage.size.height * 0.2);
    
    UIButton *signupButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signupButton.frame = CGRectMake((_screenWidth / 2) - (signupImageWidth / 2), inputContainerView.frame.origin.y + inputContainerView.frame.size.height + 10, signupImageWidth, signupImageHeight);
    [signupButton setImage:signupImage forState:UIControlStateNormal];
    signupButton.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:signupButton];
    
    [signupButton addTarget:self action:@selector(signupEvent) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *orImage = [UIImage imageNamed:@"or"];
    float orImageWidth = orImage.size.width - (orImage.size.width * 0.2);
    float orImageHeight = orImage.size.height - (orImage.size.height * 0.2);
    
    UIImageView *orImageView = [[UIImageView alloc] initWithImage:orImage];
    orImageView.frame = CGRectMake(signupButton.frame.origin.x, signupButton.frame.origin.y + signupButton.frame.size.height + 10, orImageWidth, orImageHeight);
    //orImageView.backgroundColor = [UIColor orangeColor];
    orImageView.contentMode = UIViewContentModeScaleAspectFit;
    orImageView.clipsToBounds = YES;
    [view addSubview:orImageView];
    
    UIImage *fbSignupImage = [UIImage imageNamed:@"fb-signupButton"];
    float fbSignupImageWidth = fbSignupImage.size.width - (fbSignupImage.size.width * 0.2);
    float fbSignupImageHeight = fbSignupImage.size.height - (fbSignupImage.size.height * 0.2);
    
    UIButton *fbSignupButton = [UIButton buttonWithType:UIButtonTypeCustom];
    fbSignupButton.frame = CGRectMake((_screenWidth / 2) - (fbSignupImageWidth / 2), orImageView.frame.origin.y + orImageView.frame.size.height + 10, fbSignupImageWidth, fbSignupImageHeight);
    [fbSignupButton setImage:fbSignupImage forState:UIControlStateNormal];
    fbSignupButton.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:fbSignupButton];
    
    [fbSignupButton addTarget:self action:@selector(fbSignupEvent) forControlEvents:UIControlEventTouchUpInside];
}

-(void)initSecondViewLayout:(UIView*)view{
    
    UIView *inputContainerView = [[UIView alloc] initWithFrame:CGRectMake(20, _registrationHeaderLabel.frame.origin.y + _registrationHeaderLabel.frame.size.height + 10, _registrationHeaderLabel.frame.size.width, 45 * 4)];
    //inputContainerView.backgroundColor = [UIColor greenColor];
    inputContainerView.layer.cornerRadius = 6;
    [view addSubview:inputContainerView];
    
    _mobileNumberTextField = [[cUITextField alloc] initWithFrame:CGRectMake(0, 0, inputContainerView.frame.size.width, 40)];
    _mobileNumberTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _mobileNumberTextField.text = @"Mobile Number";
    _mobileNumberTextField.textColor = [UIColor grayColor];
    _mobileNumberTextField.font = [UIFont systemFontOfSize:15];
    _mobileNumberTextField.textAlignment = NSTextAlignmentLeft;
    _mobileNumberTextField.layer.cornerRadius = 6;
    [inputContainerView addSubview:_mobileNumberTextField];
    
    _countryTextField = [[cUITextField alloc] initWithFrame:CGRectMake(0, _mobileNumberTextField.frame.origin.y + _mobileNumberTextField.frame.size.height + 5, inputContainerView.frame.size.width, 40)];
    _countryTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _countryTextField.text = @"Country";
    _countryTextField.textColor = [UIColor grayColor];
    _countryTextField.font = [UIFont systemFontOfSize:15];
    _countryTextField.textAlignment = NSTextAlignmentLeft;
    _countryTextField.layer.cornerRadius = 6;
    [inputContainerView addSubview:_countryTextField];
    
     _birthdayTextField = [[cUITextField alloc] initWithFrame:CGRectMake(0, _countryTextField.frame.origin.y + _countryTextField.frame.size.height + 5, inputContainerView.frame.size.width, 40)];
    _birthdayTextField.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _birthdayTextField.text = @"Birthday";
    _birthdayTextField.textColor = [UIColor grayColor];
    _birthdayTextField.font = [UIFont systemFontOfSize:15];
    _birthdayTextField.textAlignment = NSTextAlignmentLeft;
    _birthdayTextField.layer.cornerRadius = 6;
    [inputContainerView addSubview:_birthdayTextField];
    
    _backgroundGenderDropdown = [[UIView alloc] initWithFrame:CGRectMake(0, _birthdayTextField.frame.origin.y + _birthdayTextField.frame.size.height + 5, inputContainerView.frame.size.width, 40)];
    _backgroundGenderDropdown.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _backgroundGenderDropdown.layer.cornerRadius = 6;
    [inputContainerView addSubview:_backgroundGenderDropdown];
    
    _genderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, _birthdayTextField.frame.size.width - 20, _birthdayTextField.frame.size.height)];
    _genderLabel.text = @"Gender";
    _genderLabel.textColor = [UIColor grayColor];
    _genderLabel.font = [UIFont systemFontOfSize:15];
    _genderLabel.textAlignment = NSTextAlignmentLeft;
    [_backgroundGenderDropdown addSubview:_genderLabel];

    UIButton *genderDropDownButton = [UIButton buttonWithType:UIButtonTypeSystem];
    genderDropDownButton.frame = CGRectMake(0, 0, _backgroundGenderDropdown.frame.size.width, _backgroundGenderDropdown.frame.size.height);
    //genderButton.backgroundColor = [UIColor greenColor];
    [_backgroundGenderDropdown addSubview:genderDropDownButton];
    
    [genderDropDownButton addTarget:self action:@selector(showGenderList) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *nextImage = [UIImage imageNamed:@"nextButton"];
    float nextImageWidth = nextImage.size.width - (nextImage.size.width * 0.2);
    float nextImageHeight = nextImage.size.height - (nextImage.size.height * 0.2);
    
    _nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _nextButton.frame = CGRectMake((_screenWidth / 2) - (nextImageWidth / 2), inputContainerView.frame.origin.y + inputContainerView.frame.size.height + 30, nextImageWidth, nextImageHeight);
    [_nextButton setImage:nextImage forState:UIControlStateNormal];
    _nextButton.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:_nextButton];
    
    _genderListView = [[UIView alloc] initWithFrame:CGRectMake(inputContainerView.frame.origin.x, (inputContainerView.frame.origin.y + inputContainerView.frame.size.height) - (_backgroundGenderDropdown.frame.size.height + 5), _backgroundGenderDropdown.frame.size.width, _backgroundGenderDropdown.frame.size.height * 4)];
    _genderListView.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.5];
    _genderListView.hidden = YES;
    _genderListView.layer.cornerRadius = 6;
    [view addSubview:_genderListView];
    
    UIImageView *dropDownImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrowDown"]];
    dropDownImageView.frame = CGRectMake((inputContainerView.frame.origin.x + inputContainerView.frame.size.width) - (_backgroundGenderDropdown.frame.size.height - 10), (inputContainerView.frame.origin.y + inputContainerView.frame.size.height) - (_backgroundGenderDropdown.frame.size.height), _backgroundGenderDropdown.frame.size.height - 20, _backgroundGenderDropdown.frame.size.height - 20);
    dropDownImageView.contentMode = UIViewContentModeScaleAspectFill;
    dropDownImageView.clipsToBounds = YES;
    [view addSubview:dropDownImageView];
    
    _genderListArray = @[@"Gender",@"Male",@"Female",@"Unspecified"];
    
    for (int i = 0; i < _genderListArray.count; i++) {
        UILabel *genderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, i * _backgroundGenderDropdown.frame.size.height, _backgroundGenderDropdown.frame.size.width - 20, _backgroundGenderDropdown.frame.size.height)];
        genderLabel.text = [_genderListArray objectAtIndex:i];
        genderLabel.textColor = [UIColor grayColor];
        genderLabel.font = [UIFont systemFontOfSize:15];
        genderLabel.textAlignment = NSTextAlignmentLeft;
        [_genderListView addSubview:genderLabel];
        
        UIButton *genderButton = [UIButton buttonWithType:UIButtonTypeSystem];
        genderButton.frame = CGRectMake(0, i * _backgroundGenderDropdown.frame.size.height, _backgroundGenderDropdown.frame.size.width, _backgroundGenderDropdown.frame.size.height);
        //genderButton.backgroundColor = [UIColor greenColor];
        genderButton.tag = i;
        [_genderListView addSubview:genderButton];
        
        [genderButton addTarget:self action:@selector(selectGender:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [_nextButton addTarget:self action:@selector(signupEvent) forControlEvents:UIControlEventTouchUpInside];
}

-(void)initThirdViewLayout:(UIView*)view{
    
    NSArray *allInterestArray = @[@"Arts", @"Food", @"Fitness", @"Crafts", @"Beauty", @"Photography",
                                  @"Training & Development", @"Music", @"Technology", @"Fashion", @"Dance"];
    
    NSMutableArray *allLabelInterestArray = [[NSMutableArray alloc] init];
    NSMutableArray *allButtonInterestArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < allInterestArray.count; i++) {
        UILabel *interestLabel = [[UILabel alloc] init];
        interestLabel.text = [allInterestArray objectAtIndex:i];
        interestLabel.textAlignment = NSTextAlignmentCenter;
        interestLabel.textColor = [UIColor whiteColor];
        interestLabel.font = [UIFont systemFontOfSize:14];
        interestLabel.layer.cornerRadius = 15;
        interestLabel.layer.borderWidth = 1;
        interestLabel.layer.borderColor = [UIColor whiteColor].CGColor;
        interestLabel.frame = CGRectMake(0, 0, interestLabel.intrinsicContentSize.width + 40, 30);
        
        [allLabelInterestArray addObject:interestLabel];
        
        UIButton *interestButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //interestButton.backgroundColor = [UIColor blueColor];
        interestButton.tag = i;
        
        [interestButton addTarget:self action:@selector(selectInterest:) forControlEvents:UIControlEventTouchUpInside];
        
        [allButtonInterestArray addObject:interestButton];
    }
    
    UIView *inputContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, _registrationHeaderLabel.frame.origin.y + _registrationHeaderLabel.frame.size.height + 30, self.view.frame.size.width, 45 * 4)];
    //inputContainerView.backgroundColor = [UIColor greenColor];
    inputContainerView.layer.cornerRadius = 6;
    [view addSubview:inputContainerView];
    
    InterestView *iv_1 = [[InterestView alloc] initWithFrame:CGRectMake(0, 0, inputContainerView.frame.size.width, 40)];
    iv_1.labelsArray = allLabelInterestArray;
    iv_1.buttonsArray = allButtonInterestArray;
    iv_1.loop = 3;
    iv_1.startIndexAt = 0;
    [inputContainerView addSubview:iv_1];
    
    InterestView *iv_2 = [[InterestView alloc] initWithFrame:CGRectMake(0, iv_1.frame.origin.y + iv_1.frame.size.height + 5, iv_1.frame.size.width, iv_1.frame.size.height)];
    iv_2.labelsArray = allLabelInterestArray;
    iv_2.buttonsArray = allButtonInterestArray;
    iv_2.loop = 3;
    iv_2.startIndexAt = 3;
    [inputContainerView addSubview:iv_2];
    
    InterestView *iv_3 = [[InterestView alloc] initWithFrame:CGRectMake(0, iv_2.frame.origin.y + iv_2.frame.size.height + 5, iv_2.frame.size.width, iv_2.frame.size.height)];
    iv_3.labelsArray = allLabelInterestArray;
    iv_3.buttonsArray = allButtonInterestArray;
    iv_3.loop = 2;
    iv_3.startIndexAt = 6;
    [inputContainerView addSubview:iv_3];
    
    InterestView *iv_4 = [[InterestView alloc] initWithFrame:CGRectMake(0, iv_3.frame.origin.y + iv_3.frame.size.height + 5, iv_3.frame.size.width, iv_3.frame.size.height)];
    iv_4.labelsArray = allLabelInterestArray;
    iv_4.buttonsArray = allButtonInterestArray;
    iv_4.loop = 3;
    iv_4.startIndexAt = 8;
    [inputContainerView addSubview:iv_4];
    
    UIImage *nextImage = [UIImage imageNamed:@"nextButton"];
    float nextImageWidth = nextImage.size.width - (nextImage.size.width * 0.2);
    float nextImageHeight = nextImage.size.height - (nextImage.size.height * 0.2);
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake((_screenWidth / 2) - (nextImageWidth / 2), inputContainerView.frame.origin.y + inputContainerView.frame.size.height + 30, nextImageWidth, nextImageHeight);
    [nextButton setImage:nextImage forState:UIControlStateNormal];
    nextButton.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:nextButton];
    
    [nextButton addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
}

/*
 * Delegates
 */

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    
    NSInteger counter = fabs(((scrollView.contentSize.width - scrollView.contentOffset.x) / scrollView.frame.size.width) - _headerTextArray.count);
    
    NSString * nextRegistrationHeader = [_headerTextArray objectAtIndex:counter];
    
    [UIView transitionWithView:_registrationHeaderLabel
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _registrationHeaderLabel.text = nextRegistrationHeader;
                    }completion:nil];
}

/*
 * Event
 */

-(void)showGenderList{
    
    _genderListView.hidden = NO;
    _backgroundGenderDropdown.hidden = YES;
    _nextButton.hidden = YES;
    NSLog(@"show gender list");
}

-(void)selectGender:(UIButton*)sender{
    
    NSLog(@"button = %ld",(long)sender.tag);
    
    _genderListView.hidden = YES;
    _backgroundGenderDropdown.hidden = NO;
    _nextButton.hidden = NO;
    
    _genderLabel.text = [_genderListArray objectAtIndex:sender.tag];
}

-(void)selectInterest:(UIButton*)sender{
    
    NSLog(@"Button = %ld", (long)sender.tag);
}

-(void)backToHome{
    
    NSLog(@"home");
    
    
    //[self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)signupEvent2{
    
    float width = CGRectGetWidth(_registrationPagerScrollView.frame);
    float height = CGRectGetHeight(_registrationPagerScrollView.frame);
    float newPosition = _registrationPagerScrollView.contentOffset.x + width;
    CGRect toVisible = CGRectMake(newPosition, 0, width, height);
    
    [_registrationPagerScrollView scrollRectToVisible:toVisible animated:YES];
    
    _counterNext++;
    NSString * nextRegistrationHeader = [_headerTextArray objectAtIndex:_counterNext];
    
    [UIView transitionWithView:_registrationHeaderLabel
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _registrationHeaderLabel.text = nextRegistrationHeader;
                    }completion:nil];


}
-(void)signupEvent{
    
    
    if (_counterNext < _countPage) {
        
        float width = CGRectGetWidth(_registrationPagerScrollView.frame);
        float height = CGRectGetHeight(_registrationPagerScrollView.frame);
        float newPosition = _registrationPagerScrollView.contentOffset.x + width;
        CGRect toVisible = CGRectMake(newPosition, 0, width, height);
        
        [_registrationPagerScrollView scrollRectToVisible:toVisible animated:YES];
        
        _counterNext++;
        NSString * nextRegistrationHeader = [_headerTextArray objectAtIndex:_counterNext];
        
        [UIView transitionWithView:_registrationHeaderLabel
                          duration:1.0f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            _registrationHeaderLabel.text = nextRegistrationHeader;
                        }completion:nil];
    
    }
    
    if (_counterNext == 2){
        
        NSLog(@"next _______");
        
        _zalentsApi = [[ZalentsApi alloc]init];
        
        [_zalentsApi registration:@"http://testedu.everythingdemo.com" accessCode:@"" firstName:_firstNameTextField.text lastName:_lastNameTextField.text email:_emailTextField.text password:_passwordTextField.text passwordConfirmation:_passwordTextField.text gender:_genderLabel.text mobileNumber:_mobileNumberTextField.text countryCode:_countryTextField.text birthday:_birthdayTextField.text terms:@"1" isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
            
            if(isComplete){
                
                
                NSLog(@"success");
                
                NSArray *jsonArray = [NSJSONSerialization
                                      JSONObjectWithData: data
                                      options: 0
                                      error: nil];
                NSDictionary *responseArray = [jsonArray valueForKey:@"errors"];
                
                NSString *str = [NSString stringWithFormat:@"%@",responseArray.allKeys];
                
                //            str = [str stringByReplacingOccurrencesOfString:@"(" withString:@""];
                //            str = [str stringByReplacingOccurrencesOfString:@")" withString:@""];
                //            str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
                //            str = [str stringByReplacingOccurrencesOfString:@"\r" withString:@""];
                //            str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                
                NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"] invertedSet];
                NSString *resultString = [[str componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
                NSLog (@"Result: %@", resultString);
                NSLog (@"length %lu",(unsigned long)resultString.length);
                
                
                if([resultString isEqualToString:@"null"]){
                    
                    NSLog(@"_counterNext %d",_counterNext);
                    NSLog(@"Json = %@",jsonArray );
                    
                    
                    _countPage = 5;
                    _counterNext = 1;
                    [self signupEvent2];
                    
                    
                }else{
                    
                    NSLog(@"%@ %@",resultString,[[responseArray valueForKey:resultString] objectAtIndex:0]);
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Error"
                                                                    message:[NSString stringWithFormat:@"%@ %@",resultString,[[responseArray valueForKey:resultString] objectAtIndex:0] ]
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];

                    
                }
                
            }else{
                NSLog(@"failed");
            }
            
            
            
            
        } ];
        
    }
    
 
    _counterNext++;
    
    NSLog(@"counter = %ld", (long)_counterNext);
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 0) {
        NSLog(@"ok");
        
        float width = CGRectGetWidth(_registrationPagerScrollView.frame);
        float height = CGRectGetHeight(_registrationPagerScrollView.frame);
        float newPosition = _registrationPagerScrollView.contentOffset.x - width;
        CGRect toVisible = CGRectMake(newPosition, 0, width, height);
        
        [_registrationPagerScrollView scrollRectToVisible:toVisible animated:YES];
        
        _counterNext=0;
        
        NSString * nextRegistrationHeader = [_headerTextArray objectAtIndex:_counterNext];
        
        [UIView transitionWithView:_registrationHeaderLabel
                          duration:1.0f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            _registrationHeaderLabel.text = nextRegistrationHeader;
                        }completion:nil];
    }
}

-(void)fbSignupEvent{
    
    [_fbHandler fbSignUpEvent:^(NSArray *result){

        NSLog(@"result = %@", result);
        
        NSArray *resultArray = result;
        
        _firstNameTextField.text = [resultArray valueForKey:@"first_name"];
        _lastNameTextField.text = [resultArray valueForKey:@"last_name"];
        _emailTextField.text = [resultArray valueForKey:@"email"];
        
        if([resultArray valueForKey:@"gender"] == nil){
            
            _genderLabel.text = @"Unspecified";
            NSLog(@"unspecified");
            
        }else if([[resultArray valueForKey:@"gender"] isEqualToString:@"male"]){
            
            _genderLabel.text = @"Male";
            NSLog(@"%@",[resultArray valueForKey:@"gender"]);
            
        }else if([[resultArray valueForKey:@"gender"] isEqualToString:@"female"]){
            
            _genderLabel.text = @"Female";
            NSLog(@"%@",[resultArray valueForKey:@"gender"]);
            
        }
    }];
}
@end
